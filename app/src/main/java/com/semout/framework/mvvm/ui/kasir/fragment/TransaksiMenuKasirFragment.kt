package com.semout.framework.mvvm.ui.kasir.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.data.local.dao.TrxPenjualanTempDao
import com.semout.framework.mvvm.data.model.local.db.TrxPenjualanTemp
import com.semout.framework.mvvm.databinding.FragmentTrxNotaBinding
import com.semout.framework.mvvm.databinding.ItemProdukTrxKasirBinding
import com.semout.framework.mvvm.ui.kasir.KasirIconActivity
import com.semout.framework.mvvm.ui.kasir.viewmodels.KasirViewModel
import com.semout.framework.mvvm.utils.NumberUtils
import com.semout.framework.mvvm.utils.adapter.GenericAdapter
import com.semout.framework.mvvm.utils.ext.observe
import org.koin.android.viewmodel.ext.android.sharedViewModel

class TransaksiMenuKasirFragment : BaseFragment() {
    private val mKasirViewModel by sharedViewModel<KasirViewModel>()
    lateinit var binding: FragmentTrxNotaBinding
    var listTrxJual: ArrayList<TrxPenjualanTemp?>? = null
    var totalNota = "0"
    private var kodeTrx: String = ""
    private var totalTrx: String = ""
    lateinit var mAdapter: GenericAdapter<TrxPenjualanTemp?, ItemProdukTrxKasirBinding?>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTrxNotaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        actionUi()
        initData()
    }

    private fun initUi() {
        listTrxJual = ArrayList()
        mAdapter =
            object :
                GenericAdapter<TrxPenjualanTemp?, ItemProdukTrxKasirBinding?>(
                    requireContext(),
                    listTrxJual!!
                ) {
                override fun getLayoutResId(): Int {
                    return R.layout.item_produk_trx_kasir
                }

                override fun onRetry() {}
                override fun noConnection() {}
                override fun setTitle(): String {
                    return "Nota Penjualan"
                }

                @SuppressLint("SetTextI18n")
                override fun onBindData(
                    model: TrxPenjualanTemp?,
                    position: Int,
                    dataBinding: ItemProdukTrxKasirBinding?
                ) {
                    val diskon =
                        if (model!!.diskon > 0) " - (${model.diskon})" else ""

                    val diskon_persen =
                        if (model.diskon > 0)
                            NumberUtils.presentasePotHarga(
                                model.diskon,
                                model.harga
                            )
                        else 0.0
                    val diskon_persen_info =
                        if (model.diskon > 0)
                            "[${
                                NumberUtils.presentasePotHarga(
                                    model.diskon,
                                    model.harga
                                ).toString().replace(".0", "")}]"
                        else ""
//                    val _nominal=NumberUtils.convertNumber(model.reharga.toString())
                    // hbelibersih = hbeli-persen-nominal
                    val harga_akr = model.harga - (model.diskon + (model.harga - diskon_persen))
                    dataBinding!!.tvNamaProduk.text =
                        "${model.namaBarang}\r\n${model.qty} * ${
                            model.harga.toString().replace(".0", "")
                        }"

                    val total_akr =
                        if (model.diskon > 0) model.total.toInt() -
                                (model.diskon
                                    .plus(diskon_persen)) else model.total

                    val _total = total_akr.toString().replace(".0", "")

                    totalTrx = model.total.toString().replace(".0", "")

                    dataBinding.tvHarga.text = totalTrx

                    //action del
                    dataBinding.action.setOnClickListener {
                        mKasirViewModel.penjualanQty = 1
                        mKasirViewModel.hapusProductJualTemp(model.id)
                    }
                }

                override fun onItemClick(model: TrxPenjualanTemp?, position: Int) {

                }
            }
        binding.recyclerView.adapter = mAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
    }

    private fun actionUi() {
        binding.imvKategori.setOnClickListener {
            KasirKategoriFragment().show(childFragmentManager, KasirKategoriFragment.TAG)
        }
        binding.BtnBayar.setOnClickListener {

        }
    }

    override fun observeChange() {
        observe(mKasirViewModel.mTrigerSavetoTrxJual, ::succesAddProduk)
//        observe(mKasirViewModel.totalJualTemp, ::loadTotalJual)
    }

    private fun succesAddProduk(isSuccessAdd: Boolean){
        if (isSuccessAdd){
            Log.d("cek","success Update => ${mKasirViewModel.kdTrx}")
            mKasirViewModel.loadTotalTrxJualTmp()
            mKasirViewModel.loadTrxJualTmp()
            mKasirViewModel.loadProdukSearch()
            mKasirViewModel.getListTrxPenjualanTmp(mKasirViewModel.kdTrx)
            mKasirViewModel.getTotalTrxTemp(mKasirViewModel.kdTrx)
            initData()
        }
    }

    private fun loadTotalJual(any: List<TrxPenjualanTempDao.TrxPenjualanTemporary>) {
        if (any != null)
            binding.TvTotal.text = NumberUtils.convertNumber(any[0].subtotal.toString())
    }

    private fun initData() {
        mKasirViewModel.getListTrxPenjualan()?.observe(requireActivity(), {
            if (listTrxJual!!.size > 0){ listTrxJual!!.clear() }
            Log.d("cek","success Update => $it")
            listTrxJual!!.addAll(it!!)
            mAdapter.notifyDataSetChanged()
        })
        mKasirViewModel.getTotalTempPembelian()?.observe(requireActivity(), {
            Log.d("cek", "success Update total => ${it[0].subtotal}")
            binding.TvTotal.text =
                NumberUtils.convertNumber(it[0].subtotal.toString().replace(".0", ""))
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }
}