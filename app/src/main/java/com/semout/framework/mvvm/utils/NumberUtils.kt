package com.semout.framework.mvvm.utils

import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import java.text.NumberFormat
import java.util.*

/**
 * rizmaulana 04/03/20.
 */
object NumberUtils {

    fun numberFormat(number: Int?) = number?.let {
        NumberFormat.getNumberInstance(Locale.getDefault()).format(number)
    } ?: "-"

    fun extractDigit(number: String) = Regex("[^0-9]").replace(number, "").toInt()

    fun convertNumber(currency: String?): String {
//        String[] toConvert = currency.split(".");
        val str = StringBuilder(currency)
        var i = str.length - 3
        while (i > 0) {
            str.insert(i, ".")
            i -= 3
        }
        return "$str"
    }

    fun presentasePotHarga(diskon: Double, harga: Double): Double {
        return harga - ((diskon / 100.0) * harga)
    }

    fun presentasePotHargaQty(diskon: Double, harga: Double, qty: Int): Double {
        return (harga * qty) - ((diskon / 100.0) * harga)
    }

    //TODO: Move to another class for date formatter
    fun formatTime(time: Long): String {
        val instant = Instant.ofEpochMilli(time)
        val local = instant.atZone(ZoneId.systemDefault()).toLocalDateTime()
        return local.format(DateTimeFormatter.ofPattern("dd MMMM yyyy"))
    }


    fun formatShortDate(time: Long): String {
        val instant = Instant.ofEpochMilli(time)
        val local = instant.atZone(ZoneId.systemDefault()).toLocalDateTime()
        return local.format(DateTimeFormatter.ofPattern("dd MMM"))
    }

    fun formatShortDate(time: String): String {
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        val local = LocalDate.parse(time, formatter)
        return local.format(DateTimeFormatter.ofPattern("dd MMM"))
    }

    fun formatStringDate(time: String): String {
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        val local = LocalDate.parse(time, formatter)
        return local.format(DateTimeFormatter.ofPattern("dd MMMM yyyy"))
    }


}