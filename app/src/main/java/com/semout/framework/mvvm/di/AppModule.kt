package com.semout.framework.mvvm.di

import androidx.recyclerview.widget.LinearLayoutManager
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.data.local.AppDbHelper
import com.semout.framework.mvvm.utils.DEFAULT_FONT
import com.semout.framework.mvvm.utils.rx.AppSchedulerProvider
import com.semout.framework.mvvm.utils.rx.SchedulerProvider
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.reactivex.disposables.CompositeDisposable
import org.koin.dsl.module


val appModule = module {

    single {
        CalligraphyConfig.Builder()
            .setDefaultFontPath(DEFAULT_FONT)
            .setFontAttrId(R.attr.fontPath)
            .build()
    }

    factory<SchedulerProvider> {
        AppSchedulerProvider()
    }


    factory {
        LinearLayoutManager(get())
    }

    single {
       CompositeDisposable()
    }
    single {
        AppDbHelper(get(), get())
    }

}