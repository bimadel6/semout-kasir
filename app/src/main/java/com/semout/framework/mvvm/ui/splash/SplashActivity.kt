package com.semout.framework.mvvm.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.semout.framework.mvvm.BuildConfig
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.databinding.SplashScreenBinding
import com.semout.framework.mvvm.ui.main.view.MainMenuActivity
import com.semout.framework.mvvm.ui.splash.intro.IntroActivity
import com.semout.framework.mvvm.ui.usermanagement.login.activity.LoginActivity
import com.semout.framework.mvvm.utils.ext.observe
import org.koin.android.viewmodel.ext.android.viewModel


class SplashActivity : BaseActivity() {
    private lateinit var binding: SplashScreenBinding
    private val splashViewModel by viewModel<SplashViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)
        binding = SplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initUi()
    }

    override fun observeChange() {
        startTimeCounter(true)
//        observe(splashViewModel.isLoged, ::startTimeCounter)
    }

    private fun initUi() {
        binding.TvVersionName.text = BuildConfig.VERSION_NAME
    }

    private fun startTimeCounter(any: Boolean) {
        if (any) {
            val intent = Intent(this@SplashActivity, MainMenuActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            Handler().postDelayed({
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                finish()
            }, 5000)

        }
    }
}