package com.semout.framework.mvvm.ui.main.fragment

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.databinding.FragmentsHomeBinding
import com.semout.framework.mvvm.ui.main.adapter.SliderAdapter
import com.semout.framework.mvvm.ui.main.model.SliderItem
import com.semout.framework.mvvm.ui.produk.features.DaftarHargaActivity
import com.semout.framework.mvvm.ui.produk.features.ProdukStokMainActivity
import com.semout.framework.mvvm.ui.produk.features.pembelian.PembelianActivity
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import kotlinx.android.synthetic.main.fragments_home.*

class HomeFragment : BaseFragment() {
    override fun observeChange() {}
    lateinit var binding: FragmentsHomeBinding
    lateinit var slider: ArrayList<SliderItem>

    private var adapter: SliderAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentsHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        actionUi()
    }

    private fun initUi() {
        slider = ArrayList()
        val model = SliderItem()
        model.description = ""
        model.imageDrawable = R.drawable.slider_sample_1
        model.imageUrl =
            "https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"

        val model1 = SliderItem()
        model1.description = ""
        model1.imageDrawable = R.drawable.slider_2
        model1.imageUrl =
            "https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260"
        slider.add(model1)
        slider.add(model)
        adapter = SliderAdapter(requireContext())
        adapter!!.addData(slider)

        imageSlider.setSliderAdapter(adapter!!);
        imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        imageSlider.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH;
        imageSlider.indicatorSelectedColor = Color.WHITE;
        imageSlider.indicatorUnselectedColor = Color.GRAY;
        imageSlider.scrollTimeInSec = 3;
        imageSlider.isAutoCycle = true;
        imageSlider.startAutoCycle()
    }

    private fun actionUi() {
        binding.cvMenuProduk.setOnClickListener {
            val i = Intent(requireContext(), DaftarHargaActivity::class.java)
            startActivity(i)
        }

        binding.cvMenuPembelian.setOnClickListener {
            val i = Intent(requireContext(), PembelianActivity::class.java)
            startActivity(i)
        }

        binding.cvMenuStok.setOnClickListener {
            val i = Intent(requireContext(), ProdukStokMainActivity::class.java)
            startActivity(i)
        }

    }
}