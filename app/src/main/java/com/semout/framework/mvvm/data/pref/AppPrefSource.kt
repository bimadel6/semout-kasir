package com.semout.framework.mvvm.data.pref

import com.orhanobut.hawk.Hawk
import com.semout.framework.mvvm.utils.CacheKey

/**
 * halim
 * comrade
 */

class AppPrefSource {
    fun setEmail(email: String) = Hawk.put(CacheKey.PREF_ACCESS_EMAIL, email)
    fun getEmail() = Hawk.get(CacheKey.PREF_ACCESS_EMAIL, "")
    fun setFullName(fname: String) = Hawk.put(CacheKey.PREF_ACCESS_FULL_NAME, fname)
    fun getFullName() = Hawk.get(CacheKey.PREF_ACCESS_FULL_NAME, "")
    fun setUserName(uname: String) = Hawk.put(CacheKey.PREF_ACCESS_UNAME, uname)
    fun getUserName() = Hawk.get(CacheKey.PREF_ACCESS_UNAME, "")
    fun setPhoto(photo: String) = Hawk.put(CacheKey.PREF_ACCESS_PHOTO, photo)
    fun getPhoto() = Hawk.get(CacheKey.PREF_ACCESS_PHOTO, "")
    fun setPhone(photo: String) = Hawk.put(CacheKey.PREF_ACCESS_PHONE, photo)
    fun getPhone() = Hawk.get(CacheKey.PREF_ACCESS_PHONE, "")
    fun setUserId(uid: String) = Hawk.put(CacheKey.PREF_ACCESS_UUID, uid)
    fun getUserId() = Hawk.get(CacheKey.PREF_ACCESS_UUID, "")

    fun setRoleID(roleid: String) = Hawk.put(CacheKey.PREF_ACCESS_USER_ROLE, roleid)
    fun getRoleID() = Hawk.get(CacheKey.PREF_ACCESS_USER_ROLE, "")
    fun setTrxPembelian(trx_beli: String) = Hawk.put(CacheKey.PREF_ACCESS_TRX_PEMBELIAN, trx_beli)
    fun getTrxPembelian() = Hawk.get(CacheKey.PREF_ACCESS_TRX_PEMBELIAN, "")
}