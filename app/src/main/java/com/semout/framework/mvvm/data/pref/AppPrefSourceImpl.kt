package com.semout.framework.mvvm.data.pref

class AppPrefSourceImpl(private val pref: AppPrefSource) : PrefHelper {
    override fun getFullname(): String {
        return pref.getFullName()
    }

    override fun setFullname(fulname: String) {
        pref.setFullName(fulname)
    }

    override fun getUsername(): String {
        return pref.getUserName()
    }

    override fun setUsername(username: String) {
        pref.setUserName(username)
    }

    override fun getPhoneNumbers(): String {
        return pref.getPhone()
    }

    override fun setPhoneNumbers(Phone: String) {
        pref.setPhone(Phone)
    }

    override fun setUUID(uuid: String) {
        pref.setUserId(uuid)
    }

    override fun getUUID(): String {
        return pref.getUserId()
    }

    override fun setEmail(email: String) {
        pref.setEmail(email)
    }

    override fun getEmail(): String {
        return pref.getEmail()
    }

    override fun setPicProfile(pic: String) {
        pref.setPhoto(pic)
    }

    override fun getPicProfile(): String {
        return pref.getPhoto()
    }

    override fun setUserRole(role: String) {
        pref.setRoleID(role)
    }

    override fun getUserRole(): String {
        return pref.getRoleID()
    }

    override fun setTrxPembelian(trx_beli: String) {
        pref.setTrxPembelian(trx_beli)
    }

    override fun getTrxPembelian(): String {
        return pref.getTrxPembelian()
    }
}