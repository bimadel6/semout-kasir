package com.semout.framework.mvvm.ui.settings.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.databinding.ProfileScreenBinding
import com.semout.framework.mvvm.ui.settings.viewmodel.ProfilViewModel
import com.semout.framework.mvvm.utils.ext.loadImageRoundUrl
import org.koin.android.viewmodel.ext.android.viewModel


class ProfilActivity : BaseActivity() {
    lateinit var binding: ProfileScreenBinding
    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, ProfilActivity::class.java)
            return intent
        }
    }

    private val mainViewModel by viewModel<ProfilViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ProfileScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initUi()
        action()
    }

    private fun initUi() {
        binding.foto.loadImageRoundUrl(mainViewModel.PicObservable)
        binding.EtFullName.text = mainViewModel.usernameObservable

        val text = "11202" // Whatever you need to encode in the QR code
        val multiFormatWriter: MultiFormatWriter = MultiFormatWriter()
        try {
            val bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, 200, 200)
            val barcodeEncoder = BarcodeEncoder()
            val bitmap = barcodeEncoder.createBitmap(bitMatrix)
            binding.barcode.setImageBitmap(bitmap)
        } catch (e: WriterException) {
            e.printStackTrace()
        }

    }

    fun action() {
        binding.backBtnVerify.setOnClickListener {
            finish()
        }
    }

    override fun observeChange() {
    }
}