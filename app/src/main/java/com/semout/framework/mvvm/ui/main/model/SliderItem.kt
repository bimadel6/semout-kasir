package com.semout.framework.mvvm.ui.main.model

class SliderItem {
    var description: String? = null
    var imageUrl: String? = null
    var imageDrawable: Int? = null
}