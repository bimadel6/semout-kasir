package com.semout.framework.mvvm.ui.main.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.ui.main.viewmodel.MainViewModel
import org.koin.android.viewmodel.ext.android.viewModel


class MainActivity : BaseActivity() {
    private val mainViewModel by viewModel<MainViewModel>()
    companion object {
        private val INTENT_USER_ID = "user_id"
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, MainActivity::class.java)
//            intent.putExtra(INTENT_USER_ID, user.id)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content_main)
        setupUI()
        setupObserver()
    }

    override fun observeChange() {

    }

    private fun setupObserver() {
    }

    private fun setupUI() {
    }

}
