package com.semout.framework.mvvm.data.local.dao;
/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.semout.framework.mvvm.data.model.local.db.TrxPembelianTemp;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by amitshekhar on 07/07/17.
 */

@Dao
public interface TrxPembelianTempDao {

    @Delete
    void delete(TrxPembelianTemp user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TrxPembelianTemp trxPembelian);

    @Update
    void updateTrxPembelian(TrxPembelianTemp trxPembelian);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<TrxPembelianTemp> users);

    @Query("INSERT INTO trx_pembelian (kd_trx_pembelian,kd_produk,nama_barang,harga,qty,stok,diskon_persen,diskon_nominal,total_diskon,stok,kd_satuan,total,keterangan) SELECT kd_trx_pembelian,kd_produk,nama_barang,harga,qty,stok,diskon_persen,diskon_nominal,total_diskon,stok,kd_satuan,total,keterangan FROM trx_pembelian_temp where kd_trx_pembelian=:kd_trx")
    void saveTempToTrx(String kd_trx);

    @Query("SELECT * FROM trx_pembelian_temp")
    Flowable<List<TrxPembelianTemp>> loadAll();

    @Query("SELECT * FROM trx_pembelian_temp WHERE kd_trx_pembelian=:kode_trx")
    Single<List<TrxPembelianTemp>> loadAllTemp(String kode_trx);

    @Query("SELECT id,nama_barang,kd_satuan,kd_produk,kd_trx_pembelian,stok,harga,qty,keterangan,diskon_persen,diskon_nominal,total_diskon,SUM(total)-SUM(diskon_nominal) as total FROM trx_pembelian_temp WHERE kd_trx_pembelian=:kode_trx limit 0,1")
    Single<List<TrxPembelianTemp>> loadTotalAllTemp(String kode_trx);

    @Query("SELECT * FROM trx_pembelian_temp WHERE id IN (:userIds)")
    Single<List<TrxPembelianTemp>> loadAllByIds(int userIds);

    @Query("DELETE FROM trx_pembelian_temp")
    void deleteAll();

    @Query("DELETE FROM trx_pembelian_temp where id=:idPemb")
    void deleteById(int idPemb);
}
