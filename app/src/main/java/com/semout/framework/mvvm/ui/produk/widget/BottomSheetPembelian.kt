package com.semout.framework.mvvm.ui.produk.widget

import android.app.DatePickerDialog
import android.content.Context
import android.widget.AutoCompleteTextView
import android.widget.DatePicker
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import com.afollestad.materialdialogs.DialogBehavior
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.ModalDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.data.model.local.db.MasterSuplier
import com.semout.framework.mvvm.ui.produk.adapter.SuplierAdapter
import com.semout.framework.mvvm.utils.TimeFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

object BottomSheetPembelian {
    fun showCustomViewDialog(
        mContext: Context, Listsuplier: ArrayList<MasterSuplier?>,
        dialogBehavior: DialogBehavior = ModalDialog,
        mlistener: PembelianListener, tglIn: String, SuplierIn: String
    ) {
        val adapter: SuplierAdapter
        val dialog = MaterialDialog(mContext, dialogBehavior).show {
            title(R.string.title_set_tgl)
            customView(R.layout.bottom_view_pembelian, scrollable = true, horizontalPadding = true)
            cornerRadius(16f)
        }
        var inDateBirth = ""
        var _idsuplier: Int = 0
        var namaSuplier: String = ""
        // Setup custom view content
        val customView = dialog.getCustomView()
        adapter =
            SuplierAdapter(mContext, android.R.layout.simple_list_item_1, Listsuplier)
        val tgl: AppCompatEditText = customView.findViewById(R.id.EtTglTrx)
        val _tglIn = if (tglIn.isNotEmpty()) tglIn else ""
        tgl.setText(_tglIn)
        val apply: AppCompatButton = customView.findViewById(R.id.btnApply)
        val date_choser: ImageView = customView.findViewById(R.id.imv_date)
        val calendar = Calendar.getInstance()
        val dateListener =
            DatePickerDialog.OnDateSetListener { view: DatePicker?, year: Int, month: Int, dayOfMonth: Int ->
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, month)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val sdf = SimpleDateFormat(TimeFormat.DATE_FORMAT_REVIEW, Locale("Asia/Jakarta"))
                val sdfdefault =
                    SimpleDateFormat(TimeFormat.DATE_FORMAT_DEFAULT, Locale("Asia/Jakarta"))
                inDateBirth = sdfdefault.format(calendar.time)
                tgl.setText(sdf.format(calendar.time))
            }
        date_choser.setOnClickListener {
            val mdialog = DatePickerDialog(
                mContext,
                dateListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )
            mdialog.show()
        }
        val spin_supl: AutoCompleteTextView = customView.findViewById(R.id.TvAutoCompleteSuplier)
        spin_supl.setAdapter(adapter)
        spin_supl.threshold = 3
        val _suplierName = if (SuplierIn.isNotEmpty()) SuplierIn else ""
        spin_supl.setText(_suplierName)
        spin_supl.setOnItemClickListener { adapterView, view, i, l ->
            val selectedPoi = adapterView.adapter.getItem(i) as MasterSuplier?
            _idsuplier = selectedPoi!!.id_suplier
            namaSuplier = selectedPoi!!.nama_suplier
            spin_supl.setText(selectedPoi?.nama_suplier)
        }
        apply.setOnClickListener {
            if (inDateBirth.isNotEmpty()) {
                mlistener.setPembelianTrx(inDateBirth, _idsuplier, namaSuplier)
                dialog.cancel()
            } else {
                Toast.makeText(mContext, "Harap isi data pembelian", Toast.LENGTH_LONG).show()
            }
        }

    }

    interface PembelianListener {
        fun setPembelianTrx(tgl: String, idSuplier: Int, namaSuplier: String)
    }
}