package com.semout.framework.mvvm.di

import com.semout.framework.mvvm.ui.kasir.viewmodels.KasirViewModel
import com.semout.framework.mvvm.ui.main.viewmodel.MainViewModel
import com.semout.framework.mvvm.ui.produk.viewmodel.*
import com.semout.framework.mvvm.ui.settings.viewmodel.ProfilViewModel
import com.semout.framework.mvvm.ui.splash.SplashViewModel
import com.semout.framework.mvvm.ui.usermanagement.login.LoginViewModel
import com.semout.framework.mvvm.ui.usermanagement.register.RegisterViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Comrade45 2020-10-19
 */

val viewModelModule = module {
    viewModel { MainViewModel(get(), get()) }
    viewModel { SplashViewModel(get(), get(), get()) }
    viewModel { LoginViewModel(get(), get(), get()) }
    viewModel { RegisterViewModel(get(), get(), get()) }
    viewModel { KategoriViewModel(get()) }
    viewModel { KemasanViewModel(get()) }
    viewModel { ProdukViewModel(get(),get()) }
    viewModel { PembelianViewModel(get(),get()) }
    viewModel { StokViewModel(get()) }
    viewModel { KasirViewModel(get(),get()) }
    viewModel { ProfilViewModel(get(),get()) }
}