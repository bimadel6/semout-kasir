package com.semout.framework.mvvm.data.local.dao;
/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.semout.framework.mvvm.data.model.local.db.TrxPembelian;
import com.semout.framework.mvvm.data.model.local.db.UserLocal;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by amitshekhar on 07/07/17.
 */

@Dao
public interface TrxPembelianDao {

    @Delete
    void delete(TrxPembelian user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TrxPembelian trxPembelian);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<TrxPembelian> users);

    @Query("SELECT * FROM trx_pembelian")
    Flowable<List<TrxPembelian>> loadAll();

    @Query("SELECT * FROM trx_pembelian where kd_produk=:kd_produk ORDER BY id DESC limit 0,1")
    Flowable<List<TrxPembelian>> getHargaLast(String kd_produk);


    @Query("UPDATE trx_pembelian SET stok=:stoks WHERE id=:idPemb")
    Completable updateStokPEmbelian(int stoks , int idPemb);
    /**
     * @param kd_produk
     * untuk mengecek stok mana yg di kurangi lebih dahulu
     */
    @Query("SELECT * FROM trx_pembelian WHERE kd_produk=:kd_produk AND stok != 0 ORDER BY id ASC")
    Single<List<TrxPembelian>> getCekStokPembelian(String kd_produk);

    @Query("SELECT * FROM trx_pembelian WHERE id IN (:userIds)")
    Single<List<TrxPembelian>> loadAllByIds(int userIds);

    @Query("SELECT * FROM trx_pembelian WHERE kd_produk IN (:idProduk) ORDER BY id DESC LIMIT 0,1")
    Single<List<TrxPembelian>> getLastTrxPembelianByProduk(int idProduk);
}
