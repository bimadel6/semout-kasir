/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.semout.framework.mvvm.data.model.local.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "trx_penjualan")
public class TrxPenjualan {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "id_pembelian")
    public int id_pembelian;

    @ColumnInfo(name = "kd_trx_penjualan")
    public String kd_trx_penjualan;

    @ColumnInfo(name = "kd_trx_pembelian")
    public String kd_trx_pembelian;

    @ColumnInfo(name = "kd_produk")
    public String kdProduk;

    @ColumnInfo(name = "nama_barang")
    public String namaBarang;

    @ColumnInfo(name = "harga")
    public double harga;

    @ColumnInfo(name = "qty")
    public int qty;

    @ColumnInfo(name = "diskon")
    public double diskon;

    @ColumnInfo(name = "total")
    public float total;

    @ColumnInfo(name = "kd_satuan")
    public int kd_satuan;

    @ColumnInfo(name = "keterangan")
    public String keterangan;
    /**
     * 1.standar  ( - stok )
     * 2.icon (makanan)
     * 3.temporary
     */
    @ColumnInfo(name = "tipe")
    public int tipe;
}
