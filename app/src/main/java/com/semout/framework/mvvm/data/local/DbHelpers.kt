package com.semout.framework.mvvm.data.local

import androidx.lifecycle.LiveData
import com.semout.framework.mvvm.data.local.dao.ProdukDao
import com.semout.framework.mvvm.data.local.dao.TrxPenjualanTempDao
import com.semout.framework.mvvm.data.local.dao.TrxSearchTemp
import com.semout.framework.mvvm.data.model.local.db.*
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

@JvmSuppressWildcards
interface DbHelpers {

    //user
    fun getAllUser(): LiveData<List<UserLocal>>
    fun getUserById(id: Int): Observable<UserLocal>

    //kategori
    fun getKategoriProduk(): LiveData<List<KategoriProduk>>
    fun getKategoriProdukById(id: Int): Observable<KategoriProduk>
    fun deleteKategoriProdukById(id: Int): Observable<Boolean>
    fun saveKategoriProduk(model: KategoriProduk)

    //kemasan
    fun getKemasanProduk(): LiveData<List<Kemasan>>
    fun getKemasanProdukById(id: Int): Observable<Kemasan>
    fun deleteKemasanProdukById(id: Int): Observable<Boolean>
    fun saveKemasanProduk(model: Kemasan)

    //satuan
    fun getSatuanProduk(): LiveData<List<Satuan>>
    fun getSatuanProdukById(id: Int): Observable<Satuan>
    fun deleteSatuanProdukById(id: Int): Observable<Boolean>
    fun saveSatuanProduk(model: Satuan)

    //pelanggan
    fun getPelanggan(): Observable<List<Pelanggan>>
    fun getPelangganById(id: Int): Observable<Pelanggan>
    fun deletePelangganById(id: Int): Observable<Boolean>
    fun savePelanggan(model: Pelanggan): Observable<Boolean>

    //produk
    fun getProduk(): LiveData<List<Produk>>
    fun getProdukTemp(namaProd: String): LiveData<List<TrxSearchTemporary>>
    fun getProdukTempV2(namaProd: String): Observable<List<TrxSearchTemporary>>
    fun getProdukAll(namaProd: String): Observable<List<TrxSearchTemp.TrxSearchAll>>
    fun getListProdukTemp(limit: Int,offset: Int): Observable<List<TrxSearchTemporary>>
    fun getCheckerProdukTempV2(namaProd: String): Single<List<TrxSearchTemporary>>
    fun getProdukAll(): LiveData<List<TrxSearchTemporary>>
    fun getProdukById(id: Int): Observable<Produk>
    fun deleteProdukById(id: Int): Observable<Boolean>
    fun deleteProdukTempById(id: Int): Completable
    fun saveProduk(model: Produk)
    fun getProdukSearch(namaProd: String):LiveData<List<Produk>>
    fun getProdukSearchv2(namaProd: String):Observable<List<Produk>>

    //suplier
    fun getSuplier(): Observable<List<MasterSuplier>>
    fun getSuplierById(id: Int): Observable<MasterSuplier>
    fun deleteSuplierById(id: Int): Observable<Boolean>
    fun saveSuplier(model: MasterSuplier)

    //trxpembelianTemp
    fun getTrxPembelian(kd_trx: String): Observable<List<TrxPembelian>>
    fun getTotalTrxPembelianTmp(kd_trx: String): Observable<List<TrxPembelianTemp>>
    fun getTrxPembelianTmp(kd_trx: String): Observable<List<TrxPembelianTemp>>
    fun saveTrxPembelianTmp(model: TrxPembelianTemp)
    fun editTrxPembelianTmp(kdTrx: String, model: TrxPembelianTemp)
    fun saveTrxTempPembelian(kdTrx: String)
    fun deleteTrxPembelianTmp(id: Int)
    fun deleteAllTrxPembelianTmp()


    //trxpembelian
    fun updateStokPembelian(stokLast:Int,idPembelian:Int):Completable
    fun saveMasterPembelian(model: MasterPembelian)
    fun getHargaTerakhhirPembelianTmp(kd_trx: String): LiveData<List<TrxPembelian>>
    fun getInfoStok(): LiveData<List<ProdukDao.InfoStok>>
    fun getInfoStok(search: String): LiveData<List<ProdukDao.InfoStok>>
    fun getLastTrxPembelianByIdProduk(idProduk:Int,kd_barcode:String):Observable<List<TrxPembelian>>
    /**
     * Kasir
     */
    fun getKodeTrxJual():String

    /**
     * search kasir
     * like kd barcode,nama produk
     *
     * search temp
     */
    fun addTemporaryProduk(produk:TrxSearchTemporary): Completable
    fun getProdukJual(search: String):LiveData<List<ProdukDao.InfoStokJual>>
    fun getProdukJualBYCateg(search: Int):LiveData<List<Produk>>
    fun AddProdukJualTmp(model: TrxPenjualanTemp)
    fun getProdukJualTmp(kd_trx: String): Observable<List<TrxPenjualanTemp>>
    fun getTrxPenjualanTmp(kd_trx: String): Observable<List<TrxPenjualanTemp>>
    fun getTotalTrxJualTmp(kd_trx: String): LiveData<List<TrxPenjualanTempDao.TrxPenjualanTemporary>>
    fun getTotalTrxJualTmpV2(kd_trx: String): Observable<List<TrxPenjualanTempDao.TrxPenjualanTemporary>>
    fun getQtyJualTmp(kd_trx: String,name: String): Observable<TrxPenjualanTempDao.TrxQty>
    fun deleteProdukJualTmp(id: Int)
    fun updateQty(qty: Int,id: Int)
    fun deleteProdukJualTmpByKodeAndName(kd_trx: String,name: String)
    fun deleteAllProdukJualTmp()
    fun savePenjualanMaster(masterJual:MasterPenjualan)
    fun savePenjualanTrx(trxJual:TrxPenjualan)

}