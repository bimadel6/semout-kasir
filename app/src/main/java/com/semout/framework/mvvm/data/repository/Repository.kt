package com.semout.framework.mvvm.data.repository

import androidx.lifecycle.LiveData
import com.semout.framework.mvvm.data.local.dao.ProdukDao
import com.semout.framework.mvvm.data.local.dao.TrxPenjualanTempDao
import com.semout.framework.mvvm.data.local.dao.TrxSearchTemp
import com.semout.framework.mvvm.data.model.BaseResponse
import com.semout.framework.mvvm.data.model.BaseResponses
import com.semout.framework.mvvm.data.model.BasicResponse
import com.semout.framework.mvvm.data.model.api.UserLogin
import com.semout.framework.mvvm.data.model.local.db.*
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface Repository {
    fun getKategoriProduk(): LiveData<List<KategoriProduk>>
    fun createKategoriProduk(model: KategoriProduk)

    fun getKemasan(): LiveData<List<Kemasan>>
    fun createKemasan(model: Kemasan)

    fun getSatuan(): LiveData<List<Satuan>>
    fun createSatuan(model: Satuan)

    fun getSuplier(): Observable<List<MasterSuplier>>
    fun createSuplier(model: MasterSuplier)

    fun getProduk(): LiveData<List<Produk>>
    fun getProdukSearch(name: String): LiveData<List<Produk>>
    fun getProdukJualBYCateg(search: Int): LiveData<List<Produk>>
    fun getProdukSearchv2(name: String): Observable<List<Produk>>
    fun getProdukSearchAll(name: String): Observable<List<TrxSearchTemp.TrxSearchAll>>
    fun getProdukTemp(namaProd: String): LiveData<List<TrxSearchTemporary>>
    fun getProdukTempV2(namaProd: String): Observable<List<TrxSearchTemporary>>
    fun getCheckerProdukTempV2(namaProd: String): Single<List<TrxSearchTemporary>>
    fun getProdukTempAll(): LiveData<List<TrxSearchTemporary>>
    fun createProduk(model: Produk)

    //produk temporary
    /**
     * 1.setproduk temp to master produk
     * 2.add produk temp
     * 3.del
     */
    fun getTemporaryProduk(limit:Int,offset:Int): Observable<List<TrxSearchTemporary>>
    fun addTemporaryProduk(produk:TrxSearchTemporary): Completable
    fun delTemporaryProdukByiD(id:Int): Completable
    fun login(phone: String, pwd: String,uuid: String): Observable<BaseResponse<UserLogin>>
    fun setUname(uname: String)
    fun getUname(): String

    fun setUUID(uuid: String)
    fun getUUID(): String

    fun setPicProfile(pic: String)
    fun getPicProfile(): String

    fun setUserRole(role: String)
    fun getUserRole(): String

    //trxPembeliantmp
    fun getTotalTrxBeliTmp(kd_trx: String): Observable<List<TrxPembelianTemp>>
    fun getTrxBeliTmp(kd_trx: String): Observable<List<TrxPembelianTemp>>
    fun createPembelianTmp(model: TrxPembelianTemp)
    fun editPembelianTmp(model: TrxPembelianTemp)
    fun delPembelianTmp(id: Int)
    fun selesaiTrx(kd_trx: String)

    //trx_pembelian
    fun addPembelian(model: MasterPembelian)
    fun setTrxPembelian(trx_beli: String)
    fun getTrxPembelian(): String
    fun getCekTrxPembelian(kd_trx:String):Observable<List<TrxPembelian>>
    fun getLastTrxPembelianByIdProduk(idProduk:Int,kd_barcode:String):Observable<List<TrxPembelian>>
    fun deleteTrxPembelian()
    fun updateStokPembelian(stokLast:Int,idPembelian:Int):Completable


    fun getInfoStok(): LiveData<List<ProdukDao.InfoStok>>
    fun getInfoStok(search: String): LiveData<List<ProdukDao.InfoStok>>

    fun getProdukJual(search: String): LiveData<List<ProdukDao.InfoStokJual>>
    fun AddProdukJualTmp(model: TrxPenjualanTemp)
    fun getProdukJualTmp(kd_trx: String): Observable<List<TrxPenjualanTemp>>
    fun getTrxPenjualanTmpV2(kd_trx: String): Observable<List<TrxPenjualanTemp>>
    fun getTotalTrxJualTmp(kd_trx: String): LiveData<List<TrxPenjualanTempDao.TrxPenjualanTemporary>>
    fun getTotalTrxJualTmpv2(kd_trx: String): Observable<List<TrxPenjualanTempDao.TrxPenjualanTemporary>>
    fun getQtyJualTmp(kd_trx: String, name: String): Observable<TrxPenjualanTempDao.TrxQty>
    fun deleteProdukJualTmp(id: Int)
    fun updateQty(qty: Int, id: Int)
    fun deleteProdukJualTmpByKodeAndName(kd_trx: String, name: String)
    fun deleteAllProdukJualTmp()
    fun savePenjualanMaster(masterJual: MasterPenjualan)
    fun savePenjualanTrx(trxJual: TrxPenjualan)

    fun register(
        fullname: String,
        uname: String,
        email: String,
        phone: String,
        pwd: String,
        address: String,
    ): Observable<BaseResponses>

    fun saveSessionLogin(
        uuid: String,
        fname: String,
        uname: String,
        email: String,
        pic: String,
        phone: String,
        role: String
    )
}