package com.semout.framework.mvvm.ui.produk.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.data.model.local.db.KategoriProduk
import com.semout.framework.mvvm.databinding.FragmentTabKemasanBinding
import com.semout.framework.mvvm.databinding.ItemKategoriBinding
import com.semout.framework.mvvm.ui.produk.features.AddKategoriProdukActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.KategoriViewModel
import com.semout.framework.mvvm.utils.adapter.GenericAdapter
import org.koin.android.viewmodel.ext.android.viewModel

class KategoriFragment : BaseFragment() {
    override fun observeChange() {}
    private val mviewModel by viewModel<KategoriViewModel>()
    lateinit var binding: FragmentTabKemasanBinding
    var listKategori: ArrayList<KategoriProduk?>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTabKemasanBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        actionUi()
        initUi()
        initData()
        observeChange()
    }

    private fun initData() {
        mviewModel.getKategori()
        mviewModel.loadKategori()
        listKategori = ArrayList()
        val mAdapter =
            object :
                GenericAdapter<KategoriProduk?, ItemKategoriBinding?>(
                    requireContext(),
                    listKategori!!
                ) {
                override fun getLayoutResId(): Int {
                    return R.layout.item_kategori
                }

                override fun onRetry() {}
                override fun noConnection() {}
                override fun setTitle(): String? {
                    return "Kategori"
                }

                override fun onBindData(
                    model: KategoriProduk?,
                    position: Int,
                    dataBinding: ItemKategoriBinding?
                ) {
                    dataBinding!!.titleKategori.setText(model!!.nama_kategori)
                }

                override fun onItemClick(model: KategoriProduk?, position: Int) {
                    showSnackbarMessage(model!!.nama_kategori)
                }


            }
        binding.recyclerView.adapter = mAdapter
        binding.recyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        mviewModel.getKategori().observe(requireActivity(), {
            listKategori!!.clear()
            listKategori!!.addAll(it!!)
            mAdapter.notifyDataSetChanged()
        })

    }

    private fun initUi() {

    }

    private fun actionUi() {
        binding.BtnAddProduk.setOnClickListener {
            val i = Intent(requireContext(), AddKategoriProdukActivity::class.java)
            startActivity(i)
        }
    }
}