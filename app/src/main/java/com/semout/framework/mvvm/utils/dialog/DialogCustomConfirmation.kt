package com.semout.framework.mvvm.utils.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import com.semout.framework.mvvm.R

abstract class DialogCustomConfirmation(context: Context) {
    private var dialog: Dialog? = null
    private var changeColor = false
    private var Btncancel: AppCompatTextView? = null
    private var negativeMsg: String? = null
    private var listener: DialogItemListener? = null
    abstract val title: String?
    abstract val messages: String?
    abstract val positiveTitle: String?
    abstract val iconMessages: Int

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun preparingDialog(context: Context) {
        dialog = Dialog(context)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        //        dialog.setCancelable(true);
        val window = dialog!!.window
        if (window != null) {
            val wlp = window.attributes
            wlp.gravity = Gravity.CENTER
            wlp.flags = WindowManager.LayoutParams.FLAG_FULLSCREEN
            window.attributes = wlp
            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            window.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            dialog!!.setContentView(R.layout.dialog_confirm)
            dialog!!.setCancelable(false)
            val iconMessage = dialog!!.findViewById<ImageView>(R.id.imv_icon)
            val Tvtitle = dialog!!.findViewById<AppCompatTextView>(R.id.tv_title_dialog)
            val messageInfo = dialog!!.findViewById<AppCompatTextView>(R.id.tv_title_msg)
            iconMessage.setImageDrawable(context.resources.getDrawable(iconMessages))
            Tvtitle.text = title.toString()
            messageInfo.text = messages
            Btncancel = dialog!!.findViewById(R.id.tv_act_batalkan)
            val btn_action = dialog!!.findViewById<AppCompatButton>(R.id.BtnAction)
            btn_action.text = positiveTitle
            Btncancel!!.setOnClickListener { view: View? ->
                listener!!.OnConfirmDialog(
                    view
                )
            }
            btn_action.setOnClickListener { view: View? -> listener!!.OnConfirmDialog(view) }
            if (changeColor) btn_action.setBackgroundColor(context.resources.getColor(R.color.red_800))
        }
    }

    fun showDialog() {
        if (negativeMsg != null) {
            if (!negativeMsg!!.isEmpty()) Btncancel!!.text = negativeMsg
        }
        dialog!!.show()
    }

    fun hideDialog() {
        dialog!!.cancel()
    }

    fun setListener(listener: DialogItemListener?) {
        this.listener = listener
    }

    fun setCancelable(cancelable: Boolean) {
        dialog!!.setCancelable(cancelable)
    }

    fun setNegativeMsg(text: String?) {
        negativeMsg = text
    }

    fun setPositiveColor() {
        changeColor = true
    }

    interface DialogItemListener {
        fun OnConfirmDialog(view: View?)
    }

    init {
        preparingDialog(context)
    }
}