package com.semout.framework.mvvm.ui.produk.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.semout.framework.mvvm.core.BaseViewModel
import com.semout.framework.mvvm.data.model.local.db.KategoriProduk
import com.semout.framework.mvvm.data.model.local.db.Kemasan
import com.semout.framework.mvvm.data.model.local.db.Satuan
import com.semout.framework.mvvm.data.repository.Repository
import com.semout.framework.mvvm.utils.NetworkHelper
import kotlinx.coroutines.launch

class KemasanViewModel(
    private val mainRepository: Repository
) : BaseViewModel() {

    val mTriger: MutableLiveData<Boolean> = MutableLiveData()
    val mTrigerSatuan: MutableLiveData<Boolean> = MutableLiveData()

    val kemasan: LiveData<List<Kemasan>> = Transformations.switchMap(mTriger) {
        mainRepository.getKemasan()
    }

    val satuan: LiveData<List<Satuan>> = Transformations.switchMap(mTrigerSatuan) {
        mainRepository.getSatuan()
    }

    @JvmName("getKemasan1")
    fun getKemasan(): LiveData<List<Kemasan>> {
        return kemasan
    }

    fun getListSatuan():LiveData<List<Satuan>>{
        return satuan
    }


    fun loadKategori() {
        mTriger.value = true
    }

    fun loadSatuan() {
        mTrigerSatuan.value = true
    }

    fun addKemasan(kemasan: String, jKemasan: String) {
        val model = Kemasan()
        model.namaKemasan = kemasan
        model.JenisKemasan = jKemasan
        mainRepository.createKemasan(model)
        mTriger.postValue(true)
    }

    fun addSatuan(satuan: String, kode_satuan: String) {
        val model = Satuan()
        model.nama = satuan
        model.kode_satuan = kode_satuan
        mainRepository.createSatuan(model)
        mTriger.postValue(true)
    }
}