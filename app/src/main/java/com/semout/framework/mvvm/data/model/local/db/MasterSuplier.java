/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.semout.framework.mvvm.data.model.local.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "data_suplier")
public class MasterSuplier {

    @PrimaryKey(autoGenerate = true)
    public int id_suplier;

    @ColumnInfo(name = "nama_suplier")
    public String nama_suplier;

    @ColumnInfo(name = "alamat")
    public String alamat;

    @ColumnInfo(name = "no_tlpn")
    public String no_tlpn;

    @ColumnInfo(name = "sales_hp")
    public String sales_hp;

    @ColumnInfo(name = "nama_sales")
    public String nama_sales;

    @ColumnInfo(name = "status")
    public int status;

}
