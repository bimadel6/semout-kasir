package com.semout.framework.mvvm.ui.settings.activity

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources.NotFoundException
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.ui.main.viewmodel.MainViewModel
import com.semout.framework.mvvm.utils.gmap.MapDirectionAPI
import kotlinx.android.synthetic.main.activity_setting_alamat.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.IOException
import java.util.*


class SettingAlamatActivity : BaseActivity(), OnMapReadyCallback,
    GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    var mapView: View? = null
    private val parent_view: View? = null
    private val REQUEST_PERMISSION_LOCATION = 991
    private var gMap: GoogleMap? = null
    private var googleApiClient: GoogleApiClient? = null
    private var lastKnownLocation: Location? = null
    private var pickUpLatLang: LatLng? = null
    private val destinationLatLang: LatLng? = null
    private val directionLine: Polyline? = null
    private var pickUpMarker: Marker? = null
    private val destinationMarker: Marker? = null
    private val mainViewModel by viewModel<MainViewModel>()

    private var isMapReady = false

    companion object {
        private val INTENT_USER_ID = "user_id"
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, SettingAlamatActivity::class.java)
//            intent.putExtra(INTENT_USER_ID, user.id)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_alamat)
        setupUI()
        setupAction()
        observeChange()
    }

    private fun setupAction() {
        pickUpButton!!.setOnClickListener { //onPickUp()
            onPickUp()
        }

        back_btn.setOnClickListener { finish() }
        CvCariAlamat.setOnClickListener {
            openAutocompleteActivity(1)
        }

        pickUpText.setOnClickListener {
//            setPickUpContainer.setVisibility(View.VISIBLE)
//            setDestinationContainer.setVisibility(View.GONE)
            openAutocompleteActivity(1)
        }
        BtnaddAddress.setOnClickListener {
            val i = Intent(this@SettingAlamatActivity, SimpanAlamatActivity::class.java)
            i.putExtra("long_address", topAlamat.text.toString())
            i.putExtra("address", pickUpText.text.toString())
            startActivity(i)
        }
    }


    override fun observeChange() {

    }

    private fun setupUI() {
        val behavior: BottomSheetBehavior<*> =
            BottomSheetBehavior.from<LinearLayout>(bottom_sheet)
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
        Places.initialize(applicationContext, getString(R.string.google_maps_key))
        val placesClient = Places.createClient(this)
        pickUpContainer.setVisibility(View.VISIBLE)
        destinationContainer.setVisibility(View.GONE)
        detail.visibility = View.GONE
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, getString(R.string.google_maps_key))
        }
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.mapView) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        if (googleApiClient == null) {
            googleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        }
        val locationButton =
            (mapFragment.view?.findViewById<View>(Integer.parseInt("1"))?.parent as View).findViewById<View>(
                Integer.parseInt("2")
            )
        val rlp = locationButton.getLayoutParams() as RelativeLayout.LayoutParams
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        rlp.setMargins(0, 0, 30, 150)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        gMap = googleMap
        gMap!!.uiSettings.isMyLocationButtonEnabled = true
        gMap!!.uiSettings.isMapToolbarEnabled = true

//        val locationButton = (mapFragment.view?.findViewById<View>(Integer.parseInt("1"))?.parent as View).findViewById<View>(Integer.parseInt("2"))
//        val rlp =  locationButton.getLayoutParams() as RelativeLayout.LayoutParams
//        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
//        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
//        rlp.setMargins(0, 0, 30, 50)
        try {
            val success: Boolean = googleMap!!.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.style_json
                )
            )
            if (!success) {
                Log.e("Map", "Style parsing failed.")
            }
        } catch (e: NotFoundException) {
            Log.e(
                "Map",
                "Can't find style. Error: ",
                e
            )
        }

        isMapReady = true

        updateLastLocation(true)
    }

    private fun openAutocompleteActivity(request_code: Int) {
        val fields = Arrays.asList(Place.Field.ID, Place.Field.ADDRESS, Place.Field.LAT_LNG)
        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.FULLSCREEN, fields
        )
            .build(this)
        startActivityForResult(intent, request_code)
    }

    private fun updateLastLocation(move: Boolean) {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_PERMISSION_LOCATION
            )

        }
        lastKnownLocation = googleApiClient?.let {
            LocationServices.FusedLocationApi.getLastLocation(
                it
            )
        }
        gMap!!.isMyLocationEnabled = true
        if (lastKnownLocation != null) {
            if (move) {
                gMap!!.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            lastKnownLocation!!.getLatitude(),
                            lastKnownLocation!!.getLongitude()
                        ),
                        15f
                    )
                )
                gMap!!.animateCamera(CameraUpdateFactory.zoomTo(15f))
            }
//            fetchNearDriver(lastKnownLocation.getLatitude(), lastKnownLocation!!.getLongitude())
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION_LOCATION) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                updateLastLocation(true)
            } else {
            }
        }
    }

    override fun onConnectionFailed(connection: ConnectionResult) {
        updateLastLocation(true)
    }

    override fun onConnected(p0: Bundle?) {
        updateLastLocation(true)
    }

    override fun onConnectionSuspended(p0: Int) {
        updateLastLocation(true)
    }

    private var updateAddressCallback: Callback? = null

    private fun requestAddress(latlang: LatLng?, textView: TextView, topAlamat: AppCompatTextView) {
        if (latlang != null) {
            MapDirectionAPI.getAddress(latlang).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {}

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    if (response.isSuccessful) {
                        val json = response.body!!.string()
                        runOnUiThread {
                            try {
                                val Jobject = JSONObject(json)
                                val Jarray = Jobject.getJSONArray("results")
                                val userdata = Jarray.getJSONObject(0)
                                val address = userdata.getString("formatted_address")
                                val longname = userdata.getJSONArray("address_components").getJSONObject(0).getString("long_name")
                                textView.text = address
                                topAlamat.text = longname
                                Log.e(
                                    "TESTER",
                                    userdata.getString("formatted_address")
                                )
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        }
                    }
                }
            }.also {
                updateAddressCallback = it
            })
        }
    }


    override fun onStart() {
        googleApiClient!!.connect()
        super.onStart()
    }

    override fun onStop() {
        googleApiClient!!.disconnect()
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                val place = Autocomplete.getPlaceFromIntent(data!!)
                pickUpText.text = place.address
                val latLng = place.latLng
                if (latLng != null) {
                    gMap!!.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(latLng.latitude, latLng.longitude), 15f
                        )
                    )
                    onPickUp()
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                val status = Autocomplete.getStatusFromIntent(data!!)
                Log.i(
                    "Map",
                    status.statusMessage!!
                )
            }
        }
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                val place = Autocomplete.getPlaceFromIntent(data!!)
                destinationText.text = place.address
                val latLng = place.latLng
                if (latLng != null) {
                    gMap!!.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(latLng.latitude, latLng.longitude), 15f
                        )
                    )
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                val status = Autocomplete.getStatusFromIntent(data!!)
                Log.i(
                    "Status",
                    status.statusMessage!!
                )
            }
        }
    }

    private fun onPickUp() {
//        destinationContainer.setVisibility(View.VISIBLE)
//        pickUpContainer.setVisibility(View.GONE)
        if (pickUpMarker != null) pickUpMarker!!.remove()
        val centerPos = gMap!!.cameraPosition.target
        pickUpMarker = gMap!!.addMarker(
            MarkerOptions()
                .position(centerPos)
                .title("Pick Up")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup))
        )
        pickUpLatLang = centerPos
//        textprogress.visibility = View.VISIBLE
        requestAddress(centerPos, pickUpText, topAlamat)
//            fetchNearDriver(pickUpLatLang.latitude, pickUpLatLang.longitude)
//            requestRoute()
    }

}
