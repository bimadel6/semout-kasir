package com.semout.framework.mvvm.ui.kasir

import android.R.id.message
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import com.google.zxing.Result
import com.semout.framework.mvvm.core.BaseActivity
import me.dm7.barcodescanner.zxing.ZXingScannerView


class BarcodeScannerActivity : BaseActivity() , ZXingScannerView.ResultHandler{
    private lateinit var mScannerView: ZXingScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.barcode_scanner_screen)
        initUi()
        action()
        observeChange()
    }

    private fun initUi() {
//        binding.toolbar!!.setTitle(getString(R.string.label_kategori_produk))
        mScannerView = ZXingScannerView(this)   // Programmatically initialize the scanner view
        setContentView(mScannerView)
    }

    fun action() {

    }

    override fun observeChange() {
    }
    override fun onResume() {
        super.onResume()
        mScannerView.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView.startCamera() // Start camera on resume
    }

    override fun onPause() {
        super.onPause()
        mScannerView.stopCamera() // Stop camera on pause
    }

    override fun handleResult(rawResult: Result?) {
        // Do something with the result here
        Toast.makeText(
            this, "Contents = " + rawResult!!.getText() +
                    ", Format = " + rawResult!!.getBarcodeFormat().toString(), Toast.LENGTH_SHORT
        ).show();

        val intent = Intent()
        intent.putExtra(BARCODE_TYPE, rawResult.getText())
        setResult(2, intent)
        finish() //finishing activity

        Log.v("Scanner", rawResult!!.text) // Prints scan results
        Log.v("Scanner", rawResult.barcodeFormat.toString()) // Prints the scan format (qrcode, pdf417 etc.)

        val handler = Handler()
        handler.postDelayed(
            { mScannerView.resumeCameraPreview(this@BarcodeScannerActivity) },
            2000
        )
    }
    companion object {
        const val BARCODE_TYPE = "barcode_type"
    }
}