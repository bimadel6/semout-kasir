package com.semout.framework.mvvm.ui.produk.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.data.local.dao.ProdukDao
import com.semout.framework.mvvm.databinding.ItemStokProdukBinding

class ListStokAdapter : RecyclerView.Adapter<ListStokAdapter.ViewHolder>() {

    val listItem: MutableList<ProdukDao.InfoStok> = mutableListOf()

    inner class ViewHolder(var viewBinding: ItemStokProdukBinding) : RecyclerView.ViewHolder(viewBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListStokAdapter.ViewHolder {
        val binding =
            ItemStokProdukBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ListStokAdapter.ViewHolder, position: Int) {
        val model:ProdukDao.InfoStok = listItem[position]
        if (position == 0){
            setHeaderBg(holder.viewBinding.tvNo2)
            setHeaderBg(holder.viewBinding.TvNamaProduk2)
            setHeaderBg(holder.viewBinding.k12)
            setHeaderBg(holder.viewBinding.k22)
            setHeaderBg(holder.viewBinding.minmax2)
            setHeaderBg(holder.viewBinding.rak2)

            holder.viewBinding.tvNo2.text = "No"
            holder.viewBinding.TvNamaProduk2.text = "Nama Produk"
            holder.viewBinding.k12.text = "K1"
            holder.viewBinding.k22.text = "K2"
            holder.viewBinding.minmax2.text = "Min | Max"
            holder.viewBinding.rak2.text = "No Rak"

        }else{
            holder.viewBinding.menuHeader.visibility = View.GONE
        }

        holder.viewBinding.tvNo.text = "${position+1}"
        holder.viewBinding.TvNamaProduk.text = model.nm_produk
        holder.viewBinding.k1.text = model.stok.toString()
        holder.viewBinding.k2.text = model.total_stok.toString()
        holder.viewBinding.minmax.text = "${model.min_stok} | ${model.max_stok}"
        holder.viewBinding.rak.text = model.letak_rak

    }

    private fun setHeaderBg(view: View) {
        view.setBackgroundResource(R.drawable.table_header_cell_bg)
    }

    private fun setContentBg(view: View) {
        view.setBackgroundResource(R.drawable.table_content_cell_bg)
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    fun onUpdate(data: List<ProdukDao.InfoStok>) {
        listItem.addAll(data)
        notifyDataSetChanged()
    }

    fun onReplace(data: List<ProdukDao.InfoStok>) {
        listItem.clear()
        listItem.addAll(data)
        notifyDataSetChanged()
    }
}