package com.semout.framework.mvvm.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}