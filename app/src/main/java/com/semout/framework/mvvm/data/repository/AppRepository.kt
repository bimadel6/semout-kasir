package com.semout.framework.mvvm.data.repository

import androidx.lifecycle.LiveData
import com.semout.framework.mvvm.data.api.AppRemoteSource
import com.semout.framework.mvvm.data.local.AppDbHelper
import com.semout.framework.mvvm.data.local.dao.ProdukDao
import com.semout.framework.mvvm.data.local.dao.TrxPenjualanTempDao
import com.semout.framework.mvvm.data.local.dao.TrxSearchTemp
import com.semout.framework.mvvm.data.model.BaseResponse
import com.semout.framework.mvvm.data.model.BaseResponses
import com.semout.framework.mvvm.data.model.BasicResponse
import com.semout.framework.mvvm.data.model.api.UserLogin
import com.semout.framework.mvvm.data.model.local.db.*
import com.semout.framework.mvvm.data.pref.AppPrefSourceImpl
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.HttpException
import java.net.Inet4Address
import java.net.SocketTimeoutException
import java.net.UnknownHostException


data class Result<out T>(
    val data: T? = null,
    val error: Throwable? = null
)

fun <T> Observable<T>.responseToResult(): Observable<Result<T>> {
    return map { it.asResult() }
        .onErrorReturn {
            when (it) {
                is HttpException,
                is SocketTimeoutException,
                is UnknownHostException -> {
                    it.asErrorResult()
                }
                else -> throw it
            }
        }
}

fun <T> T.asResult(): Result<T> = Result(data = this, error = null)
fun <T> Throwable.asErrorResult(): Result<T> = Result(data = null, error = this)

open class AppRepository constructor(
    private val api: AppRemoteSource,
    private val pref: AppPrefSourceImpl,
    private val local: AppDbHelper,
) : Repository {

    override fun getKategoriProduk(): LiveData<List<KategoriProduk>> {
        return local.getKategoriProduk()
    }

    override fun createKategoriProduk(model: KategoriProduk) {
        local.saveKategoriProduk(model)
    }

    override fun getKemasan(): LiveData<List<Kemasan>> {
        return local.getKemasanProduk()
    }

    override fun createKemasan(model: Kemasan) {
        return local.saveKemasanProduk(model)
    }

    override fun getSatuan(): LiveData<List<Satuan>> {
        return local.getSatuanProduk()
    }

    override fun createSatuan(model: Satuan) {
        return local.saveSatuanProduk(model)
    }

    override fun getSuplier(): Observable<List<MasterSuplier>> {
        return local.getSuplier()
    }

    override fun createSuplier(model: MasterSuplier) {
        return local.saveSuplier(model)
    }

    override fun getProduk(): LiveData<List<Produk>> {
        return local.getProduk()
    }

    override fun getProdukSearch(name: String): LiveData<List<Produk>> {
        return local.getProdukSearch(name)
    }

    override fun getProdukSearchv2(name: String): Observable<List<Produk>> {
        return local.getProdukSearchv2(name)
    }

    override fun getProdukSearchAll(name: String): Observable<List<TrxSearchTemp.TrxSearchAll>> {
        return local.getProdukAll(name)
    }

    override fun getProdukTemp(namaProd: String): LiveData<List<TrxSearchTemporary>> {
        return local.getProdukTemp(namaProd)
    }

    override fun getProdukTempV2(namaProd: String): Observable<List<TrxSearchTemporary>> {
        return local.getProdukTempV2(namaProd)
    }

    override fun getCheckerProdukTempV2(namaProd: String): Single<List<TrxSearchTemporary>> {
        return local.getCheckerProdukTempV2(namaProd)
    }

    override fun getProdukTempAll(): LiveData<List<TrxSearchTemporary>> {
        return local.getProdukAll()
    }

    override fun createProduk(model: Produk) {
        return local.saveProduk(model)
    }

    override fun getTemporaryProduk(limit: Int, offset: Int): Observable<List<TrxSearchTemporary>> {
        return local.getListProdukTemp(limit, offset)
    }

    override fun addTemporaryProduk(produk: TrxSearchTemporary): Completable {
        return local.addTemporaryProduk(produk)
    }

    override fun delTemporaryProdukByiD(id: Int): Completable {
        return local.deleteProdukTempById(id)
    }

    override fun getTotalTrxBeliTmp(kd_trx: String): Observable<List<TrxPembelianTemp>> {
        return local.getTotalTrxPembelianTmp(kd_trx)
    }

    override fun getTrxBeliTmp(kd_trx: String): Observable<List<TrxPembelianTemp>> {
        return local.getTrxPembelianTmp(kd_trx)
    }

    override fun createPembelianTmp(model: TrxPembelianTemp) {
        local.saveTrxPembelianTmp(model)
    }

    override fun editPembelianTmp(model: TrxPembelianTemp) {
        local.editTrxPembelianTmp("", model)
    }

    override fun delPembelianTmp(id: Int) {
        local.deleteTrxPembelianTmp(id)
    }

    override fun selesaiTrx(kd_trx: String) {
        local.saveTrxTempPembelian(kd_trx)
    }

    override fun addPembelian(model: MasterPembelian) {
        local.saveMasterPembelian(model)
    }

    override fun login(
        phone: String,
        pwd: String,
        uuid: String
    ): Observable<BaseResponse<UserLogin>> {
        return api.Login(phone, pwd, uuid)
    }

    override fun setUname(uname: String) {
        pref.setUsername(uname)
    }

    override fun getUname(): String {
        return pref.getUsername()
    }

    override fun setUUID(uuid: String) {
        pref.setUUID(uuid)
    }

    override fun getUUID(): String {
        return pref.getUUID()
    }

    override fun setPicProfile(pic: String) {
        pref.setPicProfile(pic)
    }

    override fun getPicProfile(): String {
        return pref.getPicProfile()
    }

    override fun setUserRole(role: String) {
        pref.setUserRole(role)
    }

    override fun getUserRole(): String {
        return pref.getUserRole()
    }

    override fun setTrxPembelian(trx_beli: String) {
        pref.setTrxPembelian(trx_beli)
    }

    override fun getTrxPembelian(): String {
        return pref.getTrxPembelian()
    }

    override fun getCekTrxPembelian(kd_produk: String): Observable<List<TrxPembelian>> {
        return local.getTrxPembelian(kd_produk)
    }

    override fun getLastTrxPembelianByIdProduk(
        idProduk: Int,
        kd_barcode: String
    ): Observable<List<TrxPembelian>> {
        return local.getLastTrxPembelianByIdProduk(idProduk, "")
    }



    override fun deleteTrxPembelian() {
        return local.deleteAllTrxPembelianTmp()
    }

    override fun updateStokPembelian(stokLast: Int, idPembelian: Int): Completable {
        return local.updateStokPembelian(stokLast, idPembelian)
    }

    override fun getInfoStok(): LiveData<List<ProdukDao.InfoStok>> {
        return local.getInfoStok()
    }

    override fun getInfoStok(search: String): LiveData<List<ProdukDao.InfoStok>> {
        return local.getInfoStok(search)
    }

    override fun getProdukJual(search: String): LiveData<List<ProdukDao.InfoStokJual>> {
        return local.getProdukJual(search)
    }

    override fun getProdukJualBYCateg(search: Int): LiveData<List<Produk>> {
        return local.getProdukJualBYCateg(search)
    }

    override fun AddProdukJualTmp(model: TrxPenjualanTemp) {
        return local.AddProdukJualTmp(model)
    }

    override fun getProdukJualTmp(kd_trx: String): Observable<List<TrxPenjualanTemp>> {
        return local.getProdukJualTmp(kd_trx)
    }

    override fun getTrxPenjualanTmpV2(kd_trx: String): Observable<List<TrxPenjualanTemp>> {
        return local.getTrxPenjualanTmp(kd_trx)
    }

    override fun getTotalTrxJualTmp(kd_trx: String): LiveData<List<TrxPenjualanTempDao.TrxPenjualanTemporary>> {
        return local.getTotalTrxJualTmp(kd_trx)
    }

    override fun getTotalTrxJualTmpv2(kd_trx: String): Observable<List<TrxPenjualanTempDao.TrxPenjualanTemporary>> {
        return local.getTotalTrxJualTmpV2(kd_trx)
    }

    override fun getQtyJualTmp(
        kd_trx: String,
        name: String
    ): Observable<TrxPenjualanTempDao.TrxQty> {
        return local.getQtyJualTmp(kd_trx, name)
    }

    override fun deleteProdukJualTmp(id: Int) {
        return local.deleteProdukJualTmp(id)
    }

    override fun updateQty(qty: Int, id: Int) {
        return local.updateQty(qty, id)
    }

    override fun deleteProdukJualTmpByKodeAndName(kd_trx: String, name: String) {
        return local.deleteProdukJualTmpByKodeAndName(kd_trx, name)
    }

    override fun deleteAllProdukJualTmp() {
        return local.deleteAllProdukJualTmp()
    }

    override fun savePenjualanMaster(masterJual: MasterPenjualan) {
        return local.savePenjualanMaster(masterJual)
    }

    override fun savePenjualanTrx(trxJual: TrxPenjualan) {
        TODO("Not yet implemented")
    }

    override fun register(
        fullname: String,
        uname: String,
        email: String,
        phone: String,
        pwd: String,
        address: String
    ): Observable<BaseResponses> {
        return api.Register(fullname, uname, email, pwd, phone, address)
    }

    override fun saveSessionLogin(
        uuid: String,
        fname: String,
        uname: String,
        email: String,
        pic: String,
        phone: String,
        role: String
    ) {
        pref.setUUID(uuid)
        pref.setPhoneNumbers(phone)
        pref.setPicProfile(pic)
        pref.setUserRole(role)

        pref.setUsername(uname)
        pref.setEmail(email)
        pref.setFullname(fname)
    }

}