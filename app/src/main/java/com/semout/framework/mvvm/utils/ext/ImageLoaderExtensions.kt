package com.semout.framework.mvvm.utils.ext

import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.semout.framework.mvvm.R
import de.hdodenhof.circleimageview.CircleImageView

fun ImageView.loadImage(url: String, placeholder: Drawable) =
    Glide.with(this).load(url).placeholder(placeholder).into(this)

fun ImageView.loadImageRound(url: String, placeholder: CircularProgressDrawable) =
    Glide.with(this).load(url)
        .placeholder(placeholder)
        .circleCrop()
        .into(this)

fun CircleImageView.loadImageRound(url: Uri) =
    Glide.with(this).load(url)
        .placeholder(R.drawable.image_placeholder)
        .into(this)

fun CircleImageView.loadImageRoundUrl(url: String) =
    Glide.with(this).load(url)
        .placeholder(R.drawable.image_placeholder)
        .into(this)
