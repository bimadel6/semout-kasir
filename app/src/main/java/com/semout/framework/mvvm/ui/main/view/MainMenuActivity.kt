package com.semout.framework.mvvm.ui.main.view

import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.ui.kasir.KasirMainActivity
import com.semout.framework.mvvm.ui.main.fragment.CustomerService
import com.semout.framework.mvvm.ui.main.fragment.HomeFragment
import com.semout.framework.mvvm.ui.main.fragment.InboxFragment
import com.semout.framework.mvvm.ui.main.fragment.ProfileFragment
import kotlinx.android.synthetic.main.activity_content_main.*

class MainMenuActivity : BaseActivity() {
    var mBackPressed: Long = 0
    var previousSelect = 0
    private var fragmentManager: FragmentManager? = null
    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item -> //            Menu menu = navigation.getMenu();
//            menu.findItem(R.id.home).setIcon(R.drawable.ic_home);
//            menu.findItem(R.id.order).setIcon(R.drawable.ic_transaksi);
//            menu.findItem(R.id.favourite).setIcon(R.drawable.ic_favourites);
//            menu.findItem(R.id.chat).setIcon(R.drawable.ic_pesan);
//            menu.findItem(R.id.profile).setIcon(R.drawable.ic_profil);
            when (item.itemId) {
                R.id.act_bottom_home -> {
                    fab_kasir.show()
                    val homeFragment = HomeFragment()
                    navigationItemSelected(0)
//                    item.setIcon(R.drawable.ic_home_s);
                    loadFrag(homeFragment, getString(R.string.menu_home), fragmentManager)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.act_bottom_cs -> {
                    fab_kasir.hide()
                    val CsFragment = CustomerService()
                    navigationItemSelected(1)
                    //item.setIcon(R.drawable.ic_transaksi_s);
                    loadFrag(CsFragment, getString(R.string.menu_cs), fragmentManager)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.act_bottom_inbox -> {
                    fab_kasir.hide()
                    val inboxFragment = InboxFragment()
                    navigationItemSelected(2)
                    //item.setIcon(R.drawable.ic_favourite);
                    loadFrag(inboxFragment, getString(R.string.menu_chat), fragmentManager)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.act_bottom_profile -> {
                    fab_kasir.hide()
                    val pesanFragment = ProfileFragment()
                    navigationItemSelected(3)
                    //item.setIcon(R.drawable.ic_pesan_s);
                    loadFrag(pesanFragment, getString(R.string.menu_profile), fragmentManager)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content_main)
        fragmentManager = supportFragmentManager
        navigationBottom.labelVisibilityMode = LabelVisibilityMode.LABEL_VISIBILITY_LABELED
        navigationBottom.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigationBottom.itemIconTintList = ColorStateList.valueOf(Color.parseColor("#008e00"))
        val menu = navigationBottom!!.getMenu()
        //menu.findItem(R.id.home).setIcon(R.drawable.ic_home_s);
        val homeFragment = HomeFragment()
        loadFrag(homeFragment, getString(R.string.menu_home), fragmentManager)
        apikey = "AIzaSyC5fkr6gCrPvEpG6sNWmkMF4jXur3VOsAA";
        var packageInfo: PackageInfo? = null
        try {
            packageInfo = packageManager.getPackageInfo(packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        fab_kasir.setOnClickListener {
            val i = Intent(this, KasirMainActivity::class.java)
            startActivity(i)
        }
        //        Constants.versionname = packageInfo.versionName;
    }

    override fun observeChange() {
    }


    override fun onBackPressed() {
        val count = this.supportFragmentManager.backStackEntryCount
        if (count == 0) {
            if (mBackPressed + 2000 > System.currentTimeMillis()) {
                super.onBackPressed()
                return
            } else {
                clickDone()
            }
        } else {
            super.onBackPressed()
        }
    }

    fun clickDone() {
        AlertDialog.Builder(this, R.style.DialogStyle)
            .setIcon(R.mipmap.ic_launcher)
            .setTitle(getString(R.string.app_name))
            .setMessage(getString(R.string.exit))
            .setPositiveButton(getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
                dialog.dismiss()
                finish()
            }
            .setNegativeButton(getString(R.string.no)) { dialog: DialogInterface, which: Int -> dialog.dismiss() }
            .show()
    }

    fun loadFrag(f1: Fragment?, name: String?, fm: FragmentManager?) {
        for (i in 0 until fm!!.backStackEntryCount) {
            fm.popBackStack()
        }
        val ft = fm.beginTransaction()
        ft.replace(R.id.Container, f1!!, name)
        ft.commit()
    }

    fun navigationItemSelected(position: Int) {
        previousSelect = position
    }

    companion object {
        @JvmField
        var apikey: String? = ""
        var instance: MainActivity? = null
    }
}