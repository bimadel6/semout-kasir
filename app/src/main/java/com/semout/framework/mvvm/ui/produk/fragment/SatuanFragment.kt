package com.semout.framework.mvvm.ui.produk.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.data.model.local.db.Satuan
import com.semout.framework.mvvm.databinding.FragmentTabKemasanBinding
import com.semout.framework.mvvm.databinding.ItemKategoriBinding
import com.semout.framework.mvvm.ui.produk.features.AddSatuanProdukActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.KemasanViewModel
import com.semout.framework.mvvm.utils.adapter.GenericAdapter
import com.semout.framework.mvvm.utils.ext.gone
import com.semout.framework.mvvm.utils.ext.visible
import kotlinx.android.synthetic.main.fragment_tab_kemasan.*
import org.koin.android.viewmodel.ext.android.viewModel

class SatuanFragment : BaseFragment() {
    override fun observeChange() {}
    private val mviewModel by viewModel<KemasanViewModel>()
    lateinit var binding: FragmentTabKemasanBinding
    var listSatuan: ArrayList<Satuan?>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTabKemasanBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        actionUi()
        initData()
    }

    private fun initData() {
        mviewModel.getListSatuan()
        mviewModel.loadSatuan()
        listSatuan = ArrayList()
        var mAdapter =
            object :
                GenericAdapter<Satuan?, ItemKategoriBinding?>(
                    requireContext(),
                    listSatuan!!
                ) {
                override fun getLayoutResId(): Int {
                    return R.layout.item_kategori
                }

                override fun onRetry() {}
                override fun noConnection() {}
                override fun setTitle(): String {
                    return "Satuan"
                }

                override fun onBindData(
                    model: Satuan?,
                    position: Int,
                    dataBinding: ItemKategoriBinding?
                ) {
                    dataBinding!!.titleKode.visible()
                    dataBinding.titleKategori.setText(model!!.nama)
                    dataBinding.titleKode.setText(model.kode_satuan)
                }

                override fun onItemClick(model: Satuan?, position: Int) {
                    showSnackbarMessage(model!!.nama)
                }


            }
        binding.recyclerView.adapter = mAdapter
        binding.recyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        mviewModel.getListSatuan().observe(requireActivity(), {
            listSatuan!!.clear()
            listSatuan!!.addAll(it!!)
            mAdapter.notifyDataSetChanged()
        })

    }

    private fun actionUi() {
        cardView46.gone()
        binding.BtnAddProduk.setOnClickListener {
            val i = Intent(requireContext(), AddSatuanProdukActivity::class.java)
            startActivity(i)
        }
    }
}