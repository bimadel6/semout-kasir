package com.semout.framework.mvvm.di

import com.semout.framework.mvvm.data.local.AppDbHelper
import com.semout.framework.mvvm.data.repository.AppRepository
import com.semout.framework.mvvm.data.repository.Repository
import org.koin.dsl.module

/**
 * Comrade45 2020-10-19
 */


val repositoryModule = module {
    factory<Repository> {
        AppRepository(get(), get(),get())
    }

}