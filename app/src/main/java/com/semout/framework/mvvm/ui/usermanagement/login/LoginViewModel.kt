package com.semout.framework.mvvm.ui.usermanagement.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.semout.framework.mvvm.core.BaseViewModel
import com.semout.framework.mvvm.data.model.api.UserLogin
import com.semout.framework.mvvm.data.repository.Repository
import com.semout.framework.mvvm.utils.NetworkHelper
import com.semout.framework.mvvm.utils.SingleLiveEvent
import com.semout.framework.mvvm.utils.ext.addTo
import com.semout.framework.mvvm.utils.rx.SchedulerProvider

class LoginViewModel(
    private val appRepository: Repository,
    private val schedulerProvider: SchedulerProvider,
    private val networkHelper: NetworkHelper
) : BaseViewModel() {
    private val _isLogin = MutableLiveData<Boolean>()
    val isLoged: LiveData<Boolean>
        get() = _isLogin

    private val _loading = MutableLiveData<Boolean>()

    val loading: LiveData<Boolean>
        get() = _loading

    private val _verified = MutableLiveData<Boolean>()

    val isVerified: LiveData<Boolean>
        get() = _verified
    val errorMessage = SingleLiveEvent<String>()
    val successMessage = SingleLiveEvent<String>()

    /**
     * kirim kode otp
     */
    fun login(phone: String, pwd: String, uuid: String) {
        _loading.postValue(true)
        appRepository.login(phone, pwd, uuid)
            .subscribeOn(schedulerProvider.io())
            .subscribe({
                _loading.postValue(false)
                if (it.status) {
                    //simpan session
                    var mlogin = it.data
                    appRepository.saveSessionLogin(
                        mlogin.id.toString(),
                        mlogin.fullname.toString(),
                        mlogin.username.toString(),
                        mlogin.email.toString(),
                        mlogin.profil_img.toString(),
                        mlogin.phone.toString(),
                        mlogin.roleid.toString()
                    )
                    successMessage.postValue(it.message)
                    _isLogin.postValue(true)
                }
            }, {
                errorMessage.postValue(networkHelper.CatchErrors(it, "login"))
                _loading.postValue(false)
                _isLogin.postValue(false)
            })
            .addTo(compositeDisposable)
    }
}