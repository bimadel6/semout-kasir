package com.semout.framework.mvvm.ui.produk.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.data.model.local.db.Kemasan
import com.semout.framework.mvvm.databinding.FragmentTabKemasanBinding
import com.semout.framework.mvvm.databinding.ItemKategoriBinding
import com.semout.framework.mvvm.ui.produk.features.AddKemasanProdukActivity
import com.semout.framework.mvvm.ui.produk.viewmodel.KemasanViewModel
import com.semout.framework.mvvm.utils.adapter.GenericAdapter
import com.semout.framework.mvvm.utils.ext.gone
import com.semout.framework.mvvm.utils.ext.visible
import kotlinx.android.synthetic.main.fragment_tab_kemasan.*
import org.koin.android.viewmodel.ext.android.viewModel

class KemasanFragment : BaseFragment() {
    override fun observeChange() {}

    private val mviewModel by viewModel<KemasanViewModel>()
    lateinit var binding: FragmentTabKemasanBinding

    var listKemasan: ArrayList<Kemasan?>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabKemasanBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        actionUi()
        initData()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initData() {
        mviewModel.getKemasan()
        mviewModel.loadKategori()
        listKemasan = ArrayList()
        var mAdapter =
            object :
                GenericAdapter<Kemasan?, ItemKategoriBinding?>(
                    requireContext(),
                    listKemasan!!
                ) {
                override fun getLayoutResId(): Int {
                    return R.layout.item_kategori
                }

                override fun onRetry() {}
                override fun noConnection() {}
                override fun setTitle(): String? {
                    return "Kemasan"
                }

                override fun onBindData(
                    model: Kemasan?,
                    position: Int,
                    dataBinding: ItemKategoriBinding?
                ) {
                    dataBinding!!.titleKode.visible()
                    dataBinding.titleKategori.text = model!!.namaKemasan
                    dataBinding.titleKode.text = model.JenisKemasan
                }

                override fun onItemClick(model: Kemasan?, position: Int) {
                    showSnackbarMessage(model!!.namaKemasan)
                }


            }
        binding.recyclerView.adapter = mAdapter
        binding.recyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        mviewModel.getKemasan().observe(requireActivity()) {
            listKemasan!!.clear()
            listKemasan!!.addAll(it!!)
            mAdapter.notifyDataSetChanged()
        }

    }

    private fun actionUi() {
        cardView46.gone()
        binding.BtnAddProduk.setOnClickListener {
            val i = Intent(requireContext(), AddKemasanProdukActivity::class.java)
            startActivity(i)
        }
    }
}