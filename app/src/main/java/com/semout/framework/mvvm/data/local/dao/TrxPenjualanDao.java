package com.semout.framework.mvvm.data.local.dao;
/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.semout.framework.mvvm.data.model.local.db.TrxPenjualan;
import com.semout.framework.mvvm.data.model.local.db.UserLocal;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by amitshekhar on 07/07/17.
 */

@Dao
public interface TrxPenjualanDao {

    @Delete
    void delete(TrxPenjualan trxPenjualan);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TrxPenjualan trxPenjualan);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<TrxPenjualan> users);

    @Query("SELECT * FROM trx_penjualan")
    Flowable<List<TrxPenjualan>> loadAll();
}
