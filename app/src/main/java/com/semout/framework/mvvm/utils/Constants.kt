/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.semout.framework.mvvm.utils

import androidx.annotation.IntDef

/**
 * Constants used throughout the app.
 */
const val DATABASE_NAME = "semouts-db"
const val PLANT_DATA_FILENAME = "plants.json"
const val API_VERSION = "1.0.0"
const val DB_VERSION = 1
const val NETWORK_TIMEOUT = 60L
const val ERROR_MESSAGE = "Cannot proceed your request, please try again later"
const val UPDATE_ERROR_MESSAGE = "Cannot get latest update"
const val DEFAULT_FONT = "fonts/proxima/proxima_nova_regular.ttf"

object CacheKey {
    const val OVERVIEW = "cache_statistics"
    const val DAYS = "cache_days"
    const val CONFIRMED = "cache_confirmed"
    const val DEATH = "cache_death"
    const val RECOVERED = "cache_recovered"
    const val COUNTRY = "cache_country"
    const val FULL_STATS = "cache_full_details"
    const val PREF_COUNTRY = "cache_pref_country"
    const val PREF_USERNAME = "cache_pref_username"
    const val PREF_EMAIL = "cache_pref_email"
    const val PREF_NO_HP = "cache_pref_nohp"


    const val PREF_ACCESS_USER = "cache_pref_access_user"
    const val PREF_ACCESS_FULL_NAME = "cache_pref_fullname"
    const val PREF_ACCESS_UUID = "cache_pref_uuid"
    const val PREF_ACCESS_UNAME = "cache_pref_uname"
    const val PREF_ACCESS_PHOTO = "cache_pref_photo"
    const val PREF_ACCESS_EMAIL = "cache_pref_email"
    const val PREF_ACCESS_PHONE = "cache_pref_phone"
    const val PREF_ACCESS_USER_ROLE = "cache_pref_role"
    const val PREF_ACCESS_TRX_PEMBELIAN = "cache_pref_trx_beli"
}

object TransaksiType {
    const val KODE_BELI = "BL"
    const val KODE_JUAL = "JL"
}


object TipePenjualan {
    const val TIPE_STANDART = 1
    const val TIPE_ICON = 2
    const val TIPE_TEMPORARY = 3
}

@IntDef(CaseType.CONFIRMED, CaseType.DEATHS, CaseType.RECOVERED, CaseType.FULL)
@Retention(AnnotationRetention.SOURCE)
annotation class CaseTypes

object TimeFormat{
    const val TIMESTAMP_FORMAT_DEFAULT = "yyyyMMddHHmmss"
    const val TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss"
    const val DATE_FORMAT_STANDARD = "yyyy-MM-dd_HHmmss"
    const val DATE_FORMAT_MY = "MM/yyyy"
    const val DATE_FORMAT_REVIEW = "dd MMMM yyyy"
    const val DATE_FORMAT_RECOMMENDATION = "dd MMM yyyy"
    const val DATE_FORMAT_MDY = "MMMM dd yyyy"
    const val DATE_FORMAT_MDY_KOMA = "MMMM dd, yyyy"
    const val DATE_FORMAT_MDY_TIMESTAMP = "MMMM dd yyyy HH:mm:ss"
    const val DATE_FORMAT_MDY_TIMESTAMP_KOMA = "MMMM dd, yyyy HH:mm:ss"
    const val DATE_FORMAT_TIMESTAMP = "dd MMMM yyyy HH:mm:ss"
    const val DATE_FORMAT_EVENT = "dd MMMM yyyy, HH:mm"
    const val DATE_FORMAT_EVENT_MDY = "MMM dd, yyyy, HH:mm"
    const val DATE_FORMAT_EVENT_MDY_DETAIL = "MMMM dd, yyyy, HH:mm"
    const val DATE_FORMAT_EVENT_DETAIL = "dd MMMM yyyy"
    const val DATE_FORMAT_EVENT_HOUR = "HH:mm"
    const val DATE_FORMAT_EVENT_TIME = "HH:mm:ss"
    const val DATE_FORMAT_DAYNAME = "EEEE"
    const val DATE_FORMAT_YEAR = "yyyy"
    const val DATE_FORMAT_MONTH = "MM"
    const val DATE_FORMAT_DAY_OF_MONTH = "dd"
    const val DATE_FORMAT_PERFORMANCE = "dd/MM/yyyy"
    const val DATE_FORMAT_PERFORMANCE_V2 = "ddMMyyyy"
    const val DATE_FORMAT_ATTENDANCE_LIST_PERIODE = "dd MMM yyyy"
    const val DATE_FORMAT_ATTENDANCE_LIST_LEAVE = "MMM dd, yyyy"
    const val DATE_FORMAT_ATTENDANCE_LIST = "EEEE, dd MMMM yyyy"
    const val DATE_FORMAT_STANDARD_ISO = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
    const val DATE_FORMAT_STANDARD_ISO_DEFAULT = "yyyy-MM-dd'T'HH:mm:ss"
    const val DATE_FORMAT_STANDARD_ISO_Z = "yyyy-MM-dd'T'HH:mm'Z'"
    const val DATE_FORMAT_STANDARD_ISO_2 = "EEE MMM dd HH:mm:ss zzz yyyy"
    const val DATE_FORMAT_STANDARD_ISO_3 = "EEE MMM dd HH:mm:ss zzzz yyyy"
    const val TIMESTAMP_FORMAT_STANDARD = "yyyy-MM-dd HH:mm:ss"
    const val DATE_FORMAT_DEFAULT = "yyyy-MM-dd"
    const val DATE_FORMAT_DEFAULT_1 = "yyyy/MM/dd"
}

object CaseType {
    const val CONFIRMED = 0
    const val DEATHS = 1
    const val RECOVERED = 2
    const val FULL = 3
    const val API_SUCCESS = 200
    const val API_FAILED = 401
}

object IncrementStatus {
    const val FLAT = 0
    const val INCREASE = 1
    const val DECREASE = 2
}