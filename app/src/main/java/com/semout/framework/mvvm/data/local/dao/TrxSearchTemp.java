package com.semout.framework.mvvm.data.local.dao;
/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.semout.framework.mvvm.data.model.local.db.TrxSearchTemporary;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by amitshekhar on 07/07/17.
 */

@Dao
public interface TrxSearchTemp {

    @Delete
    void delete(TrxSearchTemporary trxPenjualan);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TrxSearchTemporary trxPenjualan);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insertRx(TrxSearchTemporary trxPenjualan);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<TrxSearchTemporary> users);

    @Query("SELECT * FROM trx_search_temporary")
    Flowable<List<TrxSearchTemporary>> loadAll();

    @Query("DELETE  FROM trx_search_temporary WHERE id=:id")
    Completable deleteProdukByID(int id);

    @Query("SELECT * FROM trx_search_temporary WHERE nama_produk like :namaProd")
    Flowable<List<TrxSearchTemporary>> searchProduk(String namaProd);

    @Query("SELECT * FROM trx_search_temporary")
    Observable<List<TrxSearchTemporary>> showallPagination();
//    Observable<List<TrxSearchTemporary>> showallPagination(int limit, int offset);

    @Query("SELECT * FROM trx_search_temporary WHERE nama_produk like :namaProd")
    Single<List<TrxSearchTemporary>> searchProdukv2(String namaProd);

    @Query("select id,nama as nama_produk,harga_jual as harga, 1 as tipe from produk WHERE nama_produk like :namaProd UNION ALL select id,nama_produk,harga,3 as tipe from trx_search_temporary WHERE nama_produk like :namaProd")
    Single<List<TrxSearchAll>> searchProdukAll(String namaProd);

    public static class TrxSearchAll{
        public int id;
        public String nama_produk;
        public double harga;
        public int tipe;//standart,icon,temp
    }
}
