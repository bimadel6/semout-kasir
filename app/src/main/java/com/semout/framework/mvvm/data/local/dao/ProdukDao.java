package com.semout.framework.mvvm.data.local.dao;
/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.semout.framework.mvvm.data.model.local.db.Produk;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import kotlinx.coroutines.flow.Flow;

/**
 * Created by amitshekhar on 07/07/17.
 */

@Dao
public interface ProdukDao {

    @Delete
    void delete(Produk produk);

    @Query("SELECT * FROM produk WHERE nama LIKE :name LIMIT 1")
    Single<Produk> findByName(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Produk user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Produk> produks);

    @Query("SELECT * FROM produk")
    Flowable<List<Produk>> loadAll();

    @Query("SELECT * FROM produk WHERE kd_barcode IN (:kdproduk)")
    Single<List<Produk>> loadAllBybarcode(String kdproduk);

    @Query("SELECT * FROM produk WHERE nama LIKE :nama")
    Flowable<List<Produk>> loadAllByName(String nama);

    @Query("SELECT * FROM produk WHERE nama LIKE :nama")
    Single<List<Produk>> loadAllByNamev2(String nama);

    @Query("SELECT a.*" +
            "        FROM produk a \n" +
            "        LEFT JOIN trx_pembelian b ON a.id = b.kd_produk \n" +
            "         GROUP by a.id")
    Single<List<Produk>> loadStok();

    @Query("SELECT a.id id_produk,c.jenis_kemasan as kemasan,a.nama nm_produk,a.gambar gambar,a.rak letak_rak,sum(b.stok) stok ,sum(b.qty) total_stok,a.batas_min_stok min_stok,a.batas_max_stok max_stok" +
            "        FROM produk a \n" +
            "        LEFT JOIN trx_pembelian b ON a.id = b.kd_produk \n" +
            "        LEFT JOIN kemasan c ON a.id_kemasan = c.id \n" +
            "         GROUP by a.id")
    Flowable<List<InfoStok>> loadInfoStok();

    @Query("SELECT a.id id_produk,a.nama nm_produk,a.gambar gambar,a.rak letak_rak,sum(b.stok) stok ,sum(b.qty) total_stok,a.batas_min_stok min_stok,a.batas_max_stok max_stok" +
            "        FROM produk a \n" +
            "        LEFT JOIN trx_pembelian b ON a.id = b.kd_produk \n" +
            " WHERE a.nama like :search" +
            "         GROUP by a.id")
    Flowable<List<InfoStok>> loadInfoStok(String search);

    @Query("SELECT a.id id_produk,a.nama nm_produk,a.harga_jual,a.gambar gambar,sum(b.stok) stok ,sum(b.qty) total_stok,a.batas_min_stok min_stok,a.batas_max_stok max_stok,1 as tipe" +
            "        FROM produk a \n" +
            "        LEFT JOIN trx_pembelian b ON a.id = b.kd_produk \n" +
            "        WHERE  a.kd_barcode=:search and stok != 0" +
            "        GROUP by a.id")
//a.nama like %:search or
    Flowable<List<InfoStokJual>> searchProduk(String search);

    @Query("SELECT * FROM produk WHERE id_kategori = :idCateg")
    Flowable<List<Produk>> searchProdukCategory(int idCateg);

    class InfoStok {
        public int id_produk;
        public String nm_produk;
        public String kemasan;
        public String gambar;
        public String letak_rak;
        public int stok;
        public int total_stok;
        public int max_stok;
        public int min_stok;
    }

    class InfoStokJual {
        public int id_produk;
        public String nm_produk;
        public String gambar;
        public int stok;
        public int harga_jual;
        public int total_stok;
        public int max_stok;
        public int min_stok;
        public int tipe;//temp / biasa
    }

}
