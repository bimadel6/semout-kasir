package com.semout.framework.mvvm.ui.produk.features

import android.os.Bundle
import android.os.Handler
import android.viewbinding.library.activity.viewBinding
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.databinding.AddKemasanProdukScreenBinding
import com.semout.framework.mvvm.ui.produk.viewmodel.KemasanViewModel
import kotlinx.android.synthetic.main.add_kemasan_produk_screen.*
import org.koin.android.viewmodel.ext.android.viewModel

class AddKemasanProdukActivity : BaseActivity() {
    private val mViewModel by viewModel<KemasanViewModel>()
    private val binding: AddKemasanProdukScreenBinding by viewBinding()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_kemasan_produk_screen)
        initUi()
        action()
        observeChange()
    }

    private fun initUi() {
        binding.textLabelToolbar.text = getString(R.string.label_kemasan_produk)

    }

    fun action() {
        binding.BackBtn.setOnClickListener {
            finish()
        }
        binding.BtnSaveKemasan.setOnClickListener {
            showProgress()
            mViewModel.addKemasan(
                binding.EtKemasan.text.toString(),
                binding.EtKemasanSingkatan.text.toString().uppercase()
            )
        }

        mViewModel.mTriger.observe(this) { success ->
            hideProgress()
            if (success) {
                binding.EtKemasanSingkatan.setText("")
                binding.EtKemasan.setText("")
                showSnackbarMessage("Berhasil Menyimpan")
                val handler = Handler()
                handler.postDelayed({ // Do something after 5s = 5000ms
                    finish()
                }, 2000)
            } else {
                showSnackbarMessage("Gagal Menyimpan")
            }
        }
    }

    override fun observeChange() {
    }
}