package com.semout.framework.mvvm.ui.kasir

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.databinding.ContentMainKasirIconBinding
import com.semout.framework.mvvm.ui.kasir.fragment.KasirKategoriFragment
import com.semout.framework.mvvm.ui.kasir.viewmodels.KasirViewModel
import com.semout.framework.mvvm.utils.KasirHelper
import org.koin.android.viewmodel.ext.android.viewModel

class KasirIconActivity : BaseActivity(){
    lateinit var binding: ContentMainKasirIconBinding
    private var kode_trx: String = ""
    private val mViewModel by viewModel<KasirViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ContentMainKasirIconBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initUi()
        initData()
    }

    private fun initUi() {}

    private fun initData() {
        kode_trx = KasirHelper.getKodeTrxPenjualan()
    }

    override fun observeChange() {

    }

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, KasirIconActivity::class.java)
        }
    }

     fun showKategoriFragment() {
        supportFragmentManager
            .beginTransaction()
            .disallowAddToBackStack()
            .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right)
            .add(R.id.rootCL, KasirKategoriFragment.newInstance(), KasirKategoriFragment.TAG)
            .commit()
    }
}