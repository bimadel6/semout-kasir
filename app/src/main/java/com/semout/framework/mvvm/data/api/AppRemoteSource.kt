package com.semout.framework.mvvm.data.api

import com.semout.framework.mvvm.data.model.BaseResponse
import com.semout.framework.mvvm.data.model.BaseResponses
import io.reactivex.Observable
import com.semout.framework.mvvm.data.model.BasicResponse
import com.semout.framework.mvvm.data.model.api.UserLogin

class AppRemoteSource(private val api: Api) {
    fun Login(uname: String, pwd: String, uuid: String): Observable<BaseResponse<UserLogin>> {
        return api.login(uname, pwd, uuid)
    }

    fun Register(
        fname: String,
        uname: String,
        email: String,
        pwd: String,
        phone: String,
        alamat: String
    ): Observable<BaseResponses> {
        return api.register(fname, uname, email, pwd, phone, alamat)
    }
}