package com.semout.framework.mvvm.ui.produk.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.data.local.dao.ProdukDao
import com.smarteist.autoimageslider.SliderViewAdapter
import kotlinx.android.synthetic.main.item_stok_produk.view.*


class StokProdukAdapter(
    private val context: Context
) : SliderViewAdapter<StokProdukAdapter.StokProdukAdapterVH>() {
    private val itemModel: ArrayList<ProdukDao.InfoStok?> = ArrayList()

    class StokProdukAdapterVH(itemView: View?) : SliderViewAdapter.ViewHolder(itemView) {
        var tvNo: TextView? = itemView!!.tvNo
        var tvNamaProduk: TextView? = itemView!!.TvNamaProduk
        var tvK1: TextView? = itemView!!.k1
        var tvK2: TextView? = itemView!!.k2
        var tvRak: TextView? = itemView!!.rak
        var tvMinMax: TextView? = itemView!!.minmax
    }
    private fun setHeaderBg(view: View) {
        view.setBackgroundResource(R.drawable.table_header_cell_bg)
    }

    private fun setContentBg(view: View) {
        view.setBackgroundResource(R.drawable.table_content_cell_bg)
    }
    fun addData(list: List<ProdukDao.InfoStok>) {
        itemModel.clear()
        itemModel.addAll(list)
        notifyDataSetChanged()
    }

    fun deleteItem(position: Int) {
        itemModel.removeAt(position)
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return itemModel.size + 1
    }

    override fun onCreateViewHolder(parent: ViewGroup?): StokProdukAdapterVH {
        val inflate: View = LayoutInflater.from(parent!!.context)
            .inflate(R.layout.item_stok_produk, null)
        return StokProdukAdapterVH(inflate)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(itemView: StokProdukAdapterVH?, position: Int) {
//        val rowPos = itemView.`
        if (position == 0){
            setHeaderBg(itemView!!.tvNo!!)
            setHeaderBg(itemView.tvNamaProduk!!)
            setHeaderBg(itemView.tvK1!!)
            setHeaderBg(itemView.tvK2!!)
            setHeaderBg(itemView.tvMinMax!!)
            setHeaderBg(itemView.tvRak!!)

            itemView.tvNo!!.text = "No"
            itemView.tvNamaProduk!!.text = "Nama Produk"
            itemView.tvK1!!.text = "K1"
            itemView.tvK2!!.text = "K2"
            itemView.tvMinMax!!.text = "Min | Max"
            itemView.tvRak!!.text = "No Rak"
        }else{
            val model:ProdukDao.InfoStok = itemModel[position - 1]!!
            itemView!!.tvNo!!.text = "${position+1}"
            itemView.tvNamaProduk!!.text = model.nm_produk
            itemView.tvK1!!.text = model.stok.toString()
            itemView.tvK2!!.text = model.total_stok.toString()
            itemView.tvMinMax!!.text = "${model.min_stok} | ${model.max_stok}"
            itemView.tvRak!!.text = model.letak_rak
        }
    }
}