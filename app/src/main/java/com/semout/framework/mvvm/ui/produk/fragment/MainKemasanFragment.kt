package com.semout.framework.mvvm.ui.produk.fragment

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseFragment
import com.semout.framework.mvvm.databinding.ProdukstokMainScreenBinding
import com.semout.framework.mvvm.ui.produk.viewmodel.KemasanViewModel
import com.semout.framework.mvvm.utils.common_adapter.ViewPagerAdapter
import com.semout.framework.mvvm.utils.ext.gone
import kotlinx.android.synthetic.main.produkstok_main_screen.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainKemasanFragment : BaseFragment() {
    override fun observeChange() {}

    private val mviewModel by viewModel<KemasanViewModel>()
    lateinit var binding: ProdukstokMainScreenBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ProdukstokMainScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        actionUi()
        initData()
    }

    private fun initUi() {
        setupViewPager(viewPager)
        binding.tabLayout.setupWithViewPager(viewPager)
        binding.tabLayout.setTabTextColors(
            ContextCompat.getColor(requireContext(), R.color.md_white_1000),
            ContextCompat.getColor(requireContext(), R.color.md_white_1000)
        )
//        setupTabIcons()

        try {
            // set icon color pre-selected
            val tab1 = binding.tabLayout.getTabAt(0)
            if (tab1 != null) {
//                tab1.setIcon(R.drawable.ic_baseline_kemasan_satuan_24)
                if (tab1.icon != null) {
                    tab1.icon?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)
                }
            }

            val tab2 = binding.tabLayout.getTabAt(1)
            if (tab2 != null) {
//                tab2.setIcon(R.drawable.ic_baseline_kemasan_sathitung_24)
                if (tab2.icon != null) {
                    tab2.icon?.setColorFilter(
                        ContextCompat.getColor(requireContext(), R.color.md_grey_200),
                        PorterDuff.Mode.SRC_IN
                    )
                }
            }

            binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    if (tab.icon != null) {
                        tab.icon?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {
                    if (tab.icon != null) {
                        tab.icon?.setColorFilter(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.md_grey_200
                            ), PorterDuff.Mode.SRC_IN
                        )
                    }
                }

                override fun onTabReselected(tab: TabLayout.Tab) {
                    if (tab.icon != null) {
                        tab.icon?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)
                    }
                }
            })

        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun initData() {
    }

    private fun actionUi() {
        binding.toolbar.gone()
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(childFragmentManager)
        val fragment1 = KemasanFragment()
        val fragment2 = SatuanFragment()
        adapter.addFragment(fragment1, "Kemasan")
        adapter.addFragment(fragment2, "Satuan")
        viewPager.adapter = adapter
    }
}