package com.semout.framework.mvvm.ui.settings.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.semout.framework.mvvm.R
import com.semout.framework.mvvm.core.BaseActivity
import com.semout.framework.mvvm.ui.main.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_simpan_alamat.*
import org.koin.android.viewmodel.ext.android.viewModel


class SimpanAlamatActivity : BaseActivity() {
    private val mainViewModel by viewModel<MainViewModel>()
    private var longAddress: String? = ""
    private var addrress: String? = ""

    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, SimpanAlamatActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simpan_alamat)
        initData()
        setupUI()
        setupAction()
        setupObserver()

    }

    private fun initData() {
        longAddress = intent.getStringExtra("long_address")
        addrress = intent.getStringExtra("address")

        EtFullName.setText(addrress)
        EtTglLahir.setText(longAddress)
    }

    private fun setupAction() {
        back_btn_verify.setOnClickListener({
            finish()
        })
    }

    override fun observeChange() {

    }

    private fun setupObserver() {
    }

    private fun setupUI() {
    }

}
