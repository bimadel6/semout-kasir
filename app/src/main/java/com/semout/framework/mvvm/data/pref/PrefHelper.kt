package com.semout.framework.mvvm.data.pref

interface PrefHelper {
    fun getFullname(): String
    fun setFullname(fulname: String)
    fun getUsername(): String
    fun setUsername(username: String)
    fun getPhoneNumbers(): String
    fun setPhoneNumbers(Phone: String)
    fun setUUID(uuid: String)
    fun getUUID(): String
    fun setEmail(email: String)
    fun getEmail(): String
    fun setPicProfile(pic: String)
    fun getPicProfile(): String
    fun setUserRole(role: String)
    fun getUserRole(): String
    fun setTrxPembelian(trx_beli: String)
    fun getTrxPembelian(): String
}